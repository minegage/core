#!/bin/bash

mvn clean;
mvn package;

printf "Packaging complete. Installing .jar file to local repository...\n";

DIRECTORY=./target

for F in $DIRECTORY/*-jar-with-dependencies.jar; do
	printf "Found built .jar file: ${F}"
	printf "\n"

	mvn install:install-file -Dfile=$F -DgroupId=com.cubezone.core -DartifactId=api -Dversion=1.0 -Dpackaging=jar;
done
