#!/bin/bash

mvn install:install-file -Dfile=lib/spigot-1.8.3.jar -DgroupId=com.cubezone.core -DartifactId=spigot -Dversion=1.8.3 -Dpackaging=jar

mvn install:install-file -Dfile=lib/PacketWrapper-1.8.3.jar -DgroupId=com.cubezone.core -DartifactId=packetwrapper -Dversion=1.8.3 -Dpackaging=jar

mvn install:install-file -Dfile=lib/ProtocolLib-1.8.3.jar -DgroupId=com.cubezone.core -DartifactId=protocollib -Dversion=1.8.3 -Dpackaging=jar

mvn install:install-file -Dfile=lib/PermissionsEx-1.23.1.jar -DgroupId=com.cubezone.core -DartifactId=permissionsex -Dversion=1.23.1 -Dpackaging=jar

mvn install:install-file -Dfile=lib/zPermissions-1.3.jar -DgroupId=com.cubezone.core -DartifactId=zpermissions -Dversion=1.3 -Dpackaging=jar
