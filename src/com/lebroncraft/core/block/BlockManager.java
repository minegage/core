package com.lebroncraft.core.block;


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.token.BlockToken;
import com.lebroncraft.core.common.util.UtilEffect;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;


public class BlockManager
		extends PluginModule {
		
	private static final String METADATA_KEY = "fbm";
	
	private Set<FallingBlock> falling = new HashSet<>();
	private Map<BlockToken, Integer> cracked = new HashMap<>();
	
	public BlockManager(JavaPlugin plugin) {
		super("Block Manager", plugin);
	}
	
	@EventHandler
	public void purgeBlocks(ChunkUnloadEvent event) {
		for (Entity entity : event.getChunk()
				.getEntities()) {
			if (falling.remove(entity)) {
				FallingBlock fallingBlock = (FallingBlock) entity;
				
				if (isDeleteOnDeath(fallingBlock)) {
					entity.remove();
				}
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public FallingBlock createFallingBlock(Block block, boolean deleteOnDeath) {
		Location loc = block.getLocation();
		BlockState state = block.getState();
		block.setType(Material.AIR);
		FallingBlock fallingBlock = createFallingBlock(loc, state.getType(), state.getData()
				.getData(), deleteOnDeath);
				
		return fallingBlock;
	}
	
	@SuppressWarnings("deprecation")
	public FallingBlock createFallingBlock(Location location, Material type, byte data, boolean deleteOnDeath) {
		FallingBlock fallingBlock = location.getWorld()
				.spawnFallingBlock(location, type, data);
				
		setDeleteOnDeath(fallingBlock, deleteOnDeath);
		fallingBlock.setDropItem(false);
		
		if (location.getChunk()
				.isLoaded()) {
			falling.add(fallingBlock);
		}
		
		return fallingBlock;
	}
	
	public void setDeleteOnDeath(FallingBlock fallingBlock, boolean deleteOnDeath) {
		fallingBlock.setMetadata(METADATA_KEY, new FixedMetadataValue(plugin, deleteOnDeath));
	}
	
	public boolean isDeleteOnDeath(FallingBlock fallingBlock) {
		if (!hasMetadata(fallingBlock)) {
			return false;
		}
		
		return fallingBlock.getMetadata(METADATA_KEY)
				.get(0)
				.asBoolean();
	}
	
	public boolean hasMetadata(FallingBlock fallingBlock) {
		return fallingBlock.hasMetadata(METADATA_KEY);
	}
	
	@EventHandler
	public void purgeDeadBlocks(TickEvent event) {
		if (event.isNot(Tick.TICK_1)) {
			return;
		}
		
		Iterator<FallingBlock> blocksIt = falling.iterator();
		while (blocksIt.hasNext()) {
			FallingBlock fallingBlock = blocksIt.next();
			if (fallingBlock.isDead() || !fallingBlock.isValid()) {
				
				if (isDeleteOnDeath(fallingBlock)) {
					fallingBlock.getLocation()
							.getBlock()
							.setType(Material.AIR);
				}
				
				blocksIt.remove();
			}
		}
	}
	
	public void crackBlock(Block block, int step) {
		removeCrack(block);
		
		if (step >= 1 && step <= 10) {
			BlockToken token = new BlockToken(block);
			cracked.put(token, step);
		}
		
		UtilEffect.breakAnimation(block, step);
	}
	
	public void clearCrack(Block block) {
		crackBlock(block, -1);
	}
	
	/**
	 * Because cracks will automatically go away after 20 seconds, the packet must be resent to
	 * "refresh" the cracks. 15 second tick is used instead of 20, as 20 may cause flickering
	 */
	@EventHandler
	public void refreshCrack(TickEvent event) {
		if (event.isNot(Tick.SEC_20)) {
			return;
		}
		
		for (Entry<BlockToken, Integer> entry : cracked.entrySet()) {
			Block block = entry.getKey()
					.getBlock();
			if (block == null) {
				continue;
			}
			
			UtilEffect.breakAnimation(block, entry.getValue());
		}
	}
	
	@EventHandler
	public void removeCrack(BlockBreakEvent event) {
		removeCrack(event.getBlock());
	}
	
	@EventHandler
	public void removeCrack(ChunkUnloadEvent event) {
		Iterator<Entry<BlockToken, Integer>> crackedIt = cracked.entrySet()
				.iterator();
				
		while (crackedIt.hasNext()) {
			BlockToken next = crackedIt.next()
					.getKey();
					
			Location location = next.getLocation();
			if (location == null || event.getChunk()
					.equals(location.getChunk())) {
				crackedIt.remove();
			}
		}
	}
	
	private void removeCrack(Block block) {
		Iterator<Entry<BlockToken, Integer>> crackedIt = cracked.entrySet()
				.iterator();
				
		while (crackedIt.hasNext()) {
			BlockToken token = crackedIt.next()
					.getKey();
			Block tokenBlock = token.getBlock();
			if (tokenBlock != null && tokenBlock.equals(block)) {
				crackedIt.remove();
				break;
			}
		}
	}
	
	
	
	
	
}
