package com.lebroncraft.core;


import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.chat.ChatManager;
import com.lebroncraft.core.command.CommandManager;
import com.lebroncraft.core.condition.VisibilityManager;
import com.lebroncraft.core.event.EventManager;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.move.MoveManager;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.server.ServerManager;
import com.lebroncraft.core.stats.StatManager;
import com.lebroncraft.core.ticker.Ticker;
import com.lebroncraft.core.timer.Timer;


public abstract class CorePlugin
		extends JavaPlugin {
		
	public static CorePlugin PLUGIN;
	
	protected Timer timer;
	protected CommandManager commandManager;
	protected ServerManager serverManager;
	protected MoveManager moveManager;
	protected VisibilityManager visibilityManager;
	protected Ticker ticker;
	protected EventManager eventManager;
	protected RankManager rankManager;
	protected StatManager statManager;
	protected ChatManager chatManager;
	
	@Override
	public void onEnable() {
		PLUGIN = this;
		
		L.initialize(this);
		
		this.timer = new Timer(this);
		
		this.commandManager = new CommandManager(this);
		this.serverManager = new ServerManager(this);
		this.eventManager = new EventManager(this);
		
		this.visibilityManager = new VisibilityManager(this);
		this.rankManager = new RankManager(this);
		this.moveManager = new MoveManager(this);
		this.chatManager = new ChatManager(this);
		this.statManager = new StatManager(this);
		
		this.ticker = new Ticker(this);
	}
	
	public Ticker getTicker() {
		return ticker;
	}
	
	public Timer getCharger() {
		return timer;
	}
	
	public CommandManager getCommandManager() {
		return commandManager;
	}
	
	public ServerManager getServerManager() {
		return serverManager;
	}
	
	public MoveManager getMoveManager() {
		return moveManager;
	}
	
	public VisibilityManager getVisibilityManager() {
		return visibilityManager;
	}
	
	public EventManager getEventManager() {
		return eventManager;
	}
	
	public RankManager getRankManager() {
		return rankManager;
	}
	
	public StatManager getStatManager() {
		return statManager;
	}
	
	public ChatManager getChatManager() {
		return chatManager;
	}
	
}
