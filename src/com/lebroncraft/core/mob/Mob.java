package com.lebroncraft.core.mob;


import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.event.ClickEntityEvent;


/**
 * A "wrapper" for a LivingEntity. Contains various flags for changing basic behaviour of the mob.
 * Must be loaded by a {@link MobManager}.
 */
public class Mob {
	
	protected LivingEntity entity = null;
	protected UUID uid;
	
	protected String world;
	protected Vector post;
	protected float yaw;
	protected float pitch;
	
	protected String tag;
	
	protected boolean armourInvulnerable = true;
	protected boolean flammable = false;
	protected boolean invulnerable = true;
	
	protected boolean targettable = false;
	protected boolean targetsOthers = false;
	protected boolean harmsOthers = false;
	
	protected boolean returnOnFall = true;
	
	protected Mob(UUID uid, Location location) {
		this.uid = uid;
		
		this.world = location.getWorld()
				.getName();
		this.post = location.toVector();
		this.yaw = location.getYaw();
		this.pitch = location.getPitch();
	}
	
	protected void setTag(String tag) {
		this.tag = tag;
		
		if (!isEntityLoaded()) {
			throw new NullPointerException("Unable to set tag; entity is not loaded");
		}
		
		if (entity.getPassenger() != null) {
			entity.getPassenger()
					.eject();
			entity.getPassenger()
					.remove();
		}
		
		Location loc = getPostLocation();
		
		ArmorStand nametag = (ArmorStand) loc.getWorld()
				.spawnEntity(loc, EntityType.ARMOR_STAND);
				
		nametag.setVisible(false);
		nametag.setSmall(true);
		nametag.setCustomName(tag);
		nametag.setCustomNameVisible(true);
		
		entity.setPassenger(nametag);
	}
	
	public void teleportToPost() {
		entity.setFallDistance(0F);
		entity.teleport(getPostLocation());
	}
	
	public LivingEntity getEntity() {
		return entity;
	}
	
	public void load(LivingEntity entity) {
		this.entity = entity;
		
		if (!flammable) {
			entity.setFireTicks(0);
		}
		
		setArmourInvulnerable(armourInvulnerable);
		
		entity.setRemoveWhenFarAway(false);
		UtilEntity.setPersistent(entity, true);
		teleportToPost();
	}
	
	public boolean isEntityLoaded() {
		return this.entity != null;
	}
	
	public boolean isPostLoaded() {
		return getPostLocation().getChunk()
				.isLoaded();
	}
	
	public void unload() {
		this.entity = null;
	}
	
	public UUID getUid() {
		return uid;
	}
	
	public void setUid(UUID uid) {
		this.uid = uid;
	}
	
	public boolean isArmourInvulnerable() {
		return armourInvulnerable;
	}
	
	public void setArmourInvulnerable(boolean armourInvulnerable) {
		this.armourInvulnerable = armourInvulnerable;
		
		EntityEquipment equipment = entity.getEquipment();
		if (equipment == null) {
			return;
		}
		
		for (ItemStack armour : equipment.getArmorContents()) {
			if (armour != null && armour.getType() != Material.AIR) {
				UtilItem.setUnbreakable(armour, armourInvulnerable);
			}
		}
	}
	
	public boolean isInvulnerable() {
		return invulnerable;
	}
	
	public void setInvulnerable(boolean invulnerable) {
		this.invulnerable = invulnerable;
	}
	
	public boolean isFlammable() {
		return flammable;
	}
	
	public void setFlammable(boolean flammable) {
		this.flammable = flammable;
	}
	
	public boolean isTargettable() {
		return targettable;
	}
	
	public void setTargettable(boolean targettable) {
		this.targettable = targettable;
	}
	
	public boolean targetsOthers() {
		return targetsOthers;
	}
	
	public void setTargetsOthers(boolean targetsOthers) {
		this.targetsOthers = targetsOthers;
	}
	
	public boolean harmsOthers() {
		return harmsOthers;
	}
	
	public void setHarmsOthers(boolean harmsOthers) {
		this.harmsOthers = harmsOthers;
	}
	
	public boolean isReturnOnFall() {
		return returnOnFall;
	}
	
	public void setReturnOnFall(boolean returnOnFall) {
		this.returnOnFall = returnOnFall;
	}
	
	public void setEntity(LivingEntity entity) {
		this.entity = entity;
	}
	
	public Vector getPost() {
		return post;
	}
	
	public Location getPostLocation() {
		Location ret = post.toLocation(Bukkit.getWorld(world));
		ret.setYaw(yaw);
		ret.setPitch(pitch);
		return ret;
	}
	
	/**
	 * @param event
	 *        The interact event fired when interacting with the mob
	 */
	public void onClick(ClickEntityEvent event) {
		// Optional override
	}
	
}
