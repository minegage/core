package com.lebroncraft.core.mob.command;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Ageable;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Villager.Profession;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.data.DataEnum;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilCommand;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.core.rank.Rank;


public class CommandMobSpawn
		extends CommandBase {
		
	public CommandMobSpawn() {
		super(Rank.ADMIN, "spawn", "create", "s", "c");
		
		addFlag("prof", Data.STRING);
		addFlag("age", Data.INTEGER);
		addFlag("ai", Data.NULL);
		addFlag("move", Data.NULL);
		addFlag("per", Data.NULL);
		addFlag("silent", Data.NULL);
		addFlag("count", Data.INTEGER);
		addFlag("name", Data.STRING);
		addFlag("namevis", Data.BOOLEAN);
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		String unparsedMob = Util.joinList(args, " ");
		
		DataEnum<MobType> mobData = new DataEnum<>(MobType.class);
		if (UtilCommand.failedParse(mobData, unparsedMob, player, "Mob", "Invalid mob type \"" + unparsedMob + "\"")) {
			return;
		}
		
		int count = 1;
		if (flags.has("count")) {
			count = flags.getInt();
		}
		
		MobType mob = mobData.getData();
		List<LivingEntity> entities = new ArrayList<>();
		
		for (int i = 0; i < count; i++) {
			entities.add(mob.spawn(player.getLocation()));
		}
		
		for (LivingEntity entity : entities) {
			entity.getEquipment()
					.setArmorContents(player.getEquipment()
							.getArmorContents());
			entity.getEquipment()
					.setItemInHand(player.getItemInHand());
		}
		
		if (flags.has("per")) {
			for (LivingEntity entity : entities) {
				UtilEntity.setPersistent(entity, true);
			}
		}
		
		if (flags.has("ai")) {
			for (LivingEntity entity : entities) {
				UtilEntity.removeAI(entity);
			}
		}
		
		if (flags.has("move")) {
			for (LivingEntity entity : entities) {
				UtilEntity.clearPathfinderGoals(entity);
			}
		}
		
		if (flags.has("target")) {
			for (LivingEntity entity : entities) {
				UtilEntity.clearTargetSelectors(entity);
			}
		}
		
		if (flags.has("silent")) {
			for (LivingEntity entity : entities) {
				UtilEntity.setSilent(entity, true);
			}
		}
		
		if (flags.has("name")) {
			for (LivingEntity entity : entities) {
				entity.setCustomName(flags.getString());
			}
		}
		
		if (flags.has("namevis")) {
			for (LivingEntity entity : entities) {
				entity.setCustomNameVisible(flags.getBoolean());
			}
		}
		
		if (flags.has("age")) {
			if (UtilCommand.notInstance(entities.get(0), Ageable.class, player, "Mob", "Flag -a requires an ageable mob")) {
				return;
			}
			
			int age = flags.getInt();
			
			
			for (LivingEntity entity : entities) {
				Ageable ageable = (Ageable) entity;
				ageable.setAge(age);
			}
		}
		
		if (flags.has("prof")) {
			if (UtilCommand.notInstance(entities.get(0), Villager.class, player, "Mob", "Flag -prof requires a villager")) {
				return;
			}
			
			String unparsedProf = flags.getString();
			
			DataEnum<Profession> profData = new DataEnum<Profession>(Profession.class);
			if (UtilCommand.failedParse(profData, unparsedProf, player, "Mob", "profession \"" + unparsedProf + "\" not found")) {
				return;
			}
			
			Profession prof = profData.getData();
			for (LivingEntity entity : entities) {
				Villager villager = (Villager) entity;
				villager.setProfession(prof);
			}
		}
		
		C.pMain(player, "Mob", "Spawned " + count + " " + mob + "(s)");
	}
}
