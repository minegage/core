package com.lebroncraft.core.mob.command;

import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;

public class CommandMob
		extends CommandBase {
	
	public CommandMob() {
		super(Rank.ADMIN, "mob", "entity");
		
		addSubCommand(new CommandMobSpawn());
		addSubCommand(new CommandMobClear());
		addSubCommand(new CommandMobList());
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		C.pHelp(player, "mob spawn", "spawns a mob");
		C.pHelp(player, "mob clear", "clears mobs");
		C.pHelp(player, "mob list", "lists mob types");
	}
	
}
