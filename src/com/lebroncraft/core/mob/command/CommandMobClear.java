package com.lebroncraft.core.mob.command;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.data.DataEnum;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilCommand;
import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.core.rank.Rank;


public class CommandMobClear
		extends CommandBase {
		
	public CommandMobClear() {
		super(Rank.ADMIN, "clear", "remove", "delete");
		addFlag("radius", Data.DOUBLE);
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		List<MobType> mobTypes = new ArrayList<>();
		if (args.size() < 1) {
			C.pMain(player, "Mob", "Please specify a mob type, or \"all\"");
			return;
		}
		
		String unparsedMob = Util.joinList(args, " ");
		
		if (unparsedMob.equalsIgnoreCase("all")) {
			for (MobType mobType : MobType.values()) {
				mobTypes.add(mobType);
			}
			mobTypes.remove(MobType.PLAYER);
		} else {
			DataEnum<MobType> mobType = new DataEnum<>(MobType.class);
			if (UtilCommand.failedParse(mobType, unparsedMob, player, "Mob", "Mob type \"" + unparsedMob + "\" not found")) {
				return;
			}
			mobTypes.add(mobType.getData());
		}
		
		double radius = Double.MAX_VALUE;
		if (flags.has("radius")) {
			radius = Math.pow(flags.getDouble(), 2);
		}
		
		Set<Entity> deleting = new HashSet<>();
		Location playerLoc = player.getLocation();
		for (Entity entity : player.getWorld()
				.getEntities()) {
			for (MobType mobType : mobTypes) {
				if (mobType.equals(entity) && playerLoc.distanceSquared(entity.getLocation()) < radius) {
					deleting.add(entity);
				}
			}
		}
		
		for (Entity entity : deleting) {
			entity.remove();
		}
		
		C.pMain(player, "Mob", "Removed " + deleting.size() + " mobs");
	}
	
}
