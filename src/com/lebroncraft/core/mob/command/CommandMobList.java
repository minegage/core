package com.lebroncraft.core.mob.command;


import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.core.rank.Rank;


public class CommandMobList
		extends CommandBase {
		
	public CommandMobList() {
		super(Rank.ADMIN, "list", "l");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		
		String message = "";
		Iterator<MobType> mobsIt = UtilJava.arrayIterator(MobType.values());
		while (mobsIt.hasNext()) {
			MobType type = mobsIt.next();
			message = message + type.toString();
			if (mobsIt.hasNext()) {
				message = message + ", ";
			}
		}
		
		C.pRaw(player, "");
		C.pRaw(player, message);
		C.pRaw(player, "");
	}
	
}
