package com.lebroncraft.core.mob.command.manager;


import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.mob.Mob;
import com.lebroncraft.core.mob.MobManager;
import com.lebroncraft.core.rank.Rank;


public abstract class CommandMobManager<T extends MobManager<M>, M extends Mob>
		extends CommandBase
		implements Listener {
		
	protected T manager;
	protected String name;
	protected SafeMap<UUID, CreateToken> attachBuffer = new SafeMap<>();
	protected Set<UUID> unattachBuffer = new HashSet<>();
	
	public CommandMobManager(T manager, String name) {
		super(Rank.ADMIN, name);
		
		this.manager = manager;
		this.name = name;
		
		manager.registerEvents(this);
		
		addSubCommand(new CommandCreate<T, M>(this));
		addSubCommand(new CommandAttach<T, M>(this));
		addSubCommand(new CommandClear(this));
		addSubCommand(new CommandUnattach(this));
	}
	
	public abstract CreateToken newToken();
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		C.pHelp(player, name + " create <mob type> [args...]", "Creates a new " + name);
		C.pHelp(player, name + " attach [args...]", "Creates a new " + name);
		C.pHelp(player, name + " unattach", "Deletes a " + name);
		C.pHelp(player, name + " clear", "Deletes all " + name + "s in the current world");
	}
	
	@EventHandler
	public void clearReference(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		UUID uid = player.getUniqueId();
		
		attachBuffer.remove(uid);
		unattachBuffer.remove(uid);
	}
	
	public void attach(Player player, LivingEntity entity, CreateToken token) {
		String serialized = token.create(entity);
		try {
			
			File file = manager.getFile(player.getWorld());
			file.createNewFile();
			
			List<String> lines = FileUtils.readLines(file);
			lines.add(serialized);
			
			FileUtils.writeLines(file, lines);
			
			C.pMain(player, name, "Entity attached");
		} catch (IOException ex) {
			C.pErr(ex, player, "Unable to attach entity");
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void clickAttach(PlayerInteractEntityEvent event) {
		Player player = event.getPlayer();
		UUID uid = player.getUniqueId();
		
		if (!attachBuffer.containsKey(uid) && !unattachBuffer.contains(uid)) {
			return;
		}
		
		Entity entity = event.getRightClicked();
		if (!( entity instanceof LivingEntity )) {
			C.pMain(player, "NPC", "The entity must be insentient");
			return;
		}
		
		LivingEntity livingEntity = (LivingEntity) entity;
		CreateToken token = attachBuffer.get(uid);
		
		if (token != null) {
			attach(player, livingEntity, token);
			attachBuffer.remove(uid);
		} else if (unattachBuffer.contains(uid)) {
			M mob = manager.getMob(livingEntity);
			
			if (mob != null) {
				try {
					manager.deleteMob(mob);
					C.pMain(player, name, "Entity unattached");
				} catch (IOException ex) {
					C.pErr(ex, player, "Unable to unattach enitty");
				}
			}
			unattachBuffer.remove(uid);
		}
		
		event.setCancelled(true);
	}
	
	public void bufferAttach(Player player, CreateToken token) {
		attachBuffer.put(player.getUniqueId(), token);
	}
	
	public void bufferUnattach(Player player) {
		unattachBuffer.add(player.getUniqueId());
	}
	
}
