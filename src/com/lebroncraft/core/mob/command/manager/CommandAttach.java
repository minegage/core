package com.lebroncraft.core.mob.command.manager;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.mob.Mob;
import com.lebroncraft.core.mob.MobManager;
import com.lebroncraft.core.rank.Rank;


public class CommandAttach<T extends MobManager<M>, M extends Mob>
		extends CommandBase {
		
	protected CommandMobManager<T, M> manager;
	
	public CommandAttach(CommandMobManager<T, M> manager) {
		super(Rank.ADMIN, "attach");
		this.manager = manager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		CreateToken token = manager.newToken();
		
		boolean success = token.create(player, args, flags);
		
		if (success) {
			manager.bufferAttach(player, token);
			C.pMain(player, manager.getName(), "Right click an entity to continue");
		}
	}
	
}
