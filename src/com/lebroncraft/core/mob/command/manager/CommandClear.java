package com.lebroncraft.core.mob.command.manager;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandClear
		extends CommandBase {
		
	private CommandMobManager<?, ?> manager;
	
	public CommandClear(CommandMobManager<?, ?> manager) {
		super(Rank.ADMIN, "clear");
		
		this.manager = manager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		manager.manager.clearMobs(player.getWorld());
		C.pMain(player, manager.getName(), "All entities cleared.");
	}
	
}
