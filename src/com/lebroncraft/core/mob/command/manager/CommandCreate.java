package com.lebroncraft.core.mob.command.manager;


import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.mob.Mob;
import com.lebroncraft.core.mob.MobManager;
import com.lebroncraft.core.mob.MobType;
import com.lebroncraft.core.rank.Rank;


public class CommandCreate<T extends MobManager<M>, M extends Mob>
		extends CommandBase {
		
	protected CommandMobManager<T, M> manager;
	
	public CommandCreate(CommandMobManager<T, M> manager) {
		super(Rank.ADMIN, "create");
		this.manager = manager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() < 1) {
			C.pMain(player, manager.name, "Please specify a mob type");
			return;
		}
		
		MobType type = UtilJava.parseEnum(MobType.class, args.get(0));
		if (type == null) {
			C.pMain(player, manager.name, "Invalid mob type, \"" + args.get(0) + "\"");
			return;
		}
		
		args.remove(0);
		
		CreateToken token = manager.newToken();
		
		boolean success = token.create(player, args, flags);
		if (success) {
			LivingEntity entity = type.spawn(player.getLocation());
			manager.attach(player, entity, token);
		}
	}
	
}
