package com.lebroncraft.core.mob.command.manager;


import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;


public abstract class CreateToken {
	
	/**
	 * @return True if successful, false otherwise.
	 */
	public abstract boolean create(Player player, List<String> args, Flags flags);
	
	public abstract String create(LivingEntity entity);
}
