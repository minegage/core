package com.lebroncraft.core.mob.command.manager;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandUnattach
		extends CommandBase {
		
	private CommandMobManager<?, ?> manager;
	
	public CommandUnattach(CommandMobManager<?, ?> manager) {
		super(Rank.ADMIN, "unattach", "delete", "remove");
		
		this.manager = manager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		manager.bufferUnattach(player);
		C.pMain(player, manager.getName(), "Click an entity to unattach it");
	}
	
}
