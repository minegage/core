package com.lebroncraft.core.mob;


import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;

import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.event.ClickEntityEvent;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;


/**
 * Manages loading/unloading of entities from a file, and determines their behaviour when they are
 * loaded.
 */
public abstract class MobManager<T extends Mob>
		extends PluginModule {
		
	protected Set<T> mobs = new HashSet<>();
	protected String filePath;
	
	public MobManager(String name, JavaPlugin plugin, String filePath) {
		super(name, plugin);
		
		if (filePath == null) {
			filePath = "null";
		}
		this.filePath = filePath;
		
		/* Loads the mobs in the worlds which are already loaded */
		for (World world : Bukkit.getWorlds()) {
			deserializeMobs(world);
			L.info("loading mobs from loaded chunks in " + world.getName());
			for (Chunk chunk : world.getLoadedChunks()) {
				manageMobs(chunk, true);
			}
		}
	}
	
	public abstract T deserializeMob(World world, String serialized);
	
	public abstract String serializeMob(T mob);
	
	/* Cache the mobs when the world is loaded */
	@EventHandler
	public final void loadMobs(WorldLoadEvent event) {
		deserializeMobs(event.getWorld());
		
		for (Chunk chunk : event.getWorld()
				.getLoadedChunks()) {
			manageMobs(chunk, true);
		}
	}
	
	
	/* Remove the mobs from the cache when the world is unloaded */
	@EventHandler
	public void unloadMobs(WorldUnloadEvent event) {
		for (Entity entity : event.getWorld()
				.getEntities()) {
				
			Iterator<T> mobsIt = mobs.iterator();
			
			while (mobsIt.hasNext()) {
				T mob = mobsIt.next();
				if (mob.getUid()
						.equals(entity.getUniqueId())) {
					mobsIt.remove();
				}
			}
		}
	}
	
	@EventHandler
	public final void loadMobs(ChunkLoadEvent event) {
		manageMobs(event.getChunk(), true);
	}
	
	@EventHandler
	public final void unloadMobs(ChunkUnloadEvent event) {
		manageMobs(event.getChunk(), false);
	}
	
	/**
	 * Loads and unloads mobs from a chunk
	 */
	private void manageMobs(Chunk chunk, boolean load) {
		for (Entity entity : chunk.getEntities()) {
			
			if (!( entity instanceof LivingEntity )) {
				continue;
			}
			
			T mob = getMob(entity);
			if (mob != null) {
				if (load) {
					L.info("loading (chunk) " + chunk.getWorld()
							.getName());
							
					mob.load((LivingEntity) entity);
				} else {
					L.info("unloading (chunk) " + chunk.getWorld()
							.getName());
					mob.unload();
				}
			}
		}
	}
	
	/**
	 * Reads from file and caches mobs (if any)
	 */
	public void deserializeMobs(World world) {
		File file = getFile(world);
		
		if (!file.exists()) {
			return;
		}
		
		if (!file.canRead()) {
			L.severe("Unable to load mobs from world \"" + world.getName() + "\"; insufficient read permissions for " + file
					.getPath());
			return;
		}
		
		try {
			List<String> fileLines = FileUtils.readLines(file);
			
			int cached = 0;
			int line = 1;
			for (String serialized : fileLines) {
				try {
					T mob = deserializeMob(world, serialized);
					registerMob(mob);
					
					cached++;
				} catch (Exception ex) {
					L.error(ex, "Failed to parse mob from " + file.getPath() + "; line " + line + ": \"" + serialized + "\"");
				}
				
				line++;
			}
			
			logInfo("Cached " + cached + " mobs from " + world.getName());
		} catch (IOException ex) {
			L.error(ex, "Unable to load mobs from world \"" + world.getName() + "\"");
		}
	}
	
	public final void saveMob(T mob) throws IOException {
		World world = mob.getEntity()
				.getWorld();
				
		File file = getFile(world);
		file.createNewFile();
		
		String serialized = serializeMob(mob);
		
		List<String> lines = FileUtils.readLines(file);
		lines.add(serialized);
		
		FileUtils.writeLines(file, lines);
	}
	
	public final void deleteMob(T mob) throws IOException {
		World world = mob.getEntity()
				.getWorld();
				
		File file = getFile(world);
		file.createNewFile();
		
		UUID uid = mob.getEntity()
				.getUniqueId();
		String uidString = uid.toString();
		
		List<String> fileLines = FileUtils.readLines(file);
		
		Iterator<String> fileIt = fileLines.iterator();
		while (fileIt.hasNext()) {
			String serialized = fileIt.next();
			if (serialized.contains(uidString)) {
				fileIt.remove();
			}
		}
		
		FileUtils.writeLines(file, fileLines);
		
		Iterator<T> mobsIt = mobs.iterator();
		while (mobsIt.hasNext()) {
			T next = mobsIt.next();
			if (next.getUid()
					.equals(uid)) {
				mobsIt.remove();
			}
		}
	}
	
	public final void clearMobs(World world) {
		getFile(world).delete();
		
		String worldName = world.getName();
		
		Iterator<T> mobsIt = mobs.iterator();
		while (mobsIt.hasNext()) {
			T mob = mobsIt.next();
			
			if (mob.world.equals(worldName)) {
				UUID uid = mob.getUid();
				
				for (Entity entity : world.getEntities()) {
					if (entity.getUniqueId()
							.equals(uid)) {
						UtilEntity.despawn(entity);
					}
				}
				
				mobsIt.remove();
			}
		}
	}
	
	public final void registerMob(T mob) {
		for (T other : mobs) {
			if (mob.getUid()
					.equals(other.getUid()) && mob.world.equals(other.world)) {
				throw new IllegalStateException("Duplicate mob " + mob.getUid());
			}
		}
		mobs.add(mob);
	}
	
	public final void removeMob(T mob) {
		mobs.remove(mob);
	}
	
	public Entity getEntity(UUID uid, World world) {
		for (Entity entity : world.getEntities()) {
			if (entity.getUniqueId()
					.equals(uid)) {
				return entity;
			}
		}
		return null;
	}
	
	public final Set<T> getMobs() {
		return mobs;
	}
	
	public final T getMob(UUID uid, World world) {
		for (T mob : mobs) {
			if (mob.getUid()
					.equals(uid) && mob.world.equals(world.getName())) {
				return mob;
			}
		}
		return null;
	}
	
	public final T getMob(Entity entity) {
		return getMob(entity.getUniqueId(), entity.getWorld());
	}
	
	public final Set<T> getMobs(World world) {
		Set<T> mobs = new HashSet<>();
		
		for (T mob : this.mobs) {
			if (mob.world.equals(world.getName())) {
				mobs.add(mob);
			}
		}
		
		return mobs;
	}
	
	public final File getFile(World world) {
		return new File(world.getWorldFolder(), filePath);
	}
	
	/* Behaviour management */
	
	@EventHandler
	public final void preventHostility(EntityTargetEvent event) {
		// Because Bukkit hates us
		if (event.getTarget() == null) {
			return;
		}
		
		T targeter = getMob(event.getEntity());
		if (targeter != null && !targeter.targetsOthers) {
			event.setCancelled(true);
			return;
		}
		
		T targetted = getMob(event.getTarget());
		if (targetted != null && !targetted.isTargettable()) {
			event.setCancelled(true);
			return;
		}
		
	}
	
	@EventHandler
	public final void preventMobDamage(EntityDamageEvent event) {
		T mob = getMob(event.getEntity());
		
		if (mob == null) {
			return;
		}
		
		if (mob.isInvulnerable()) {
			event.setCancelled(true);
		}
		
		if (event.getCause() == DamageCause.VOID) {
			if (mob.isReturnOnFall()) {
				mob.teleportToPost();
			} else {
				event.getEntity()
						.remove();
				try {
					deleteMob(mob);
				} catch (IOException ex) {
					L.error(ex, "Unable to delete mob fallen into void");
					ex.printStackTrace();
				}
			}
		}
	}
	
	@EventHandler
	public final void preventMobHarm(EntityDamageByEntityEvent event) {
		ProjectileSource damager = UtilEvent.getIndirectDamager(event);
		if (damager == null || !( damager instanceof Entity )) {
			return;
		}
		
		Entity entity = (Entity) damager;
		
		T mob = getMob(entity);
		if (mob == null) {
			return;
		}
		
		if (!mob.harmsOthers()) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public final void preventMobCombust(EntityCombustEvent event) {
		T mob = getMob(event.getEntity());
		if (mob != null && !mob.isFlammable()) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public final void removeDead(EntityDeathEvent event) {
		T mob = getMob(event.getEntity());
		if (mob != null) {
			mobs.remove(mob);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public final void onInteract(ClickEntityEvent event) {
		Entity entity = event.getClicked();
		
		T mob = getMob(entity);
		
		if (mob != null) {
			mob.onClick(event);
		}
	}
	
}
