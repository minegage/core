package com.lebroncraft.core.chat;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.chat.command.CommandChat;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilMath;
import com.lebroncraft.core.common.util.UtilNet;
import com.lebroncraft.core.common.util.UtilTime;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.timer.Timer;


public class ChatManager
		extends PluginModule {
		
	private final String URL = "http://www.purgomalum.com/service/plain?text=";
	private SafeMap<Player, Long> lastMessage = new SafeMap<>();
	
	private boolean silenced = false;
	private long silencedUntil = -1L;
	
	private boolean slow = false;
	private long slowDelay = 0L;
	
	private boolean filter = true;
	
	public ChatManager(JavaPlugin plugin) {
		super("Chat Manager", plugin);
		
		registerCommand(new CommandChat(this));
	}
	
	@EventHandler
	public void formatChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (!Timer.instance.use(player, "Chat", "Send Message", 500L, true)) {
			event.setCancelled(true);
			return;
		}
		
		Rank rank = RankManager.instance.getRank(player);
		
		if (silenced && !rank.includes(Rank.MODERATOR)) {
			if (silencedUntil == -1L) {
				C.pMain(player, "Chat", "Chat has been silenced");
			} else {
				long timeLeft = UtilTime.timeLeft(silencedUntil);
				double secondsLeft = UtilTime.toSeconds(timeLeft);
				secondsLeft = UtilMath.round(secondsLeft, 1);
				C.pMain(player, "Chat", "Chat is silenced for " + C.sOut + secondsLeft + "s");
			}
			
			event.setCancelled(true);
			return;
		}
		
		if (slow && !rank.includes(Rank.MODERATOR)) {
			long lastTime = lastMessage.get(player);
			long timeLeft = UtilTime.timeLeft(lastTime + slowDelay);
			
			if (timeLeft > 0) {
				double secondsLeft = UtilTime.toSeconds(timeLeft);
				secondsLeft = UtilMath.round(secondsLeft, 1);
				C.pMain(player, "Chat", "Chat is in slow mode. Please wait " + C.sOut + secondsLeft + "s");
				event.setCancelled(true);
				return;
			}
		}
		
		event.setFormat("%s" + C.cBold + " " + C.sDash + " " + C.cReset + "%s");
		
		if (filter && !rank.includes(Rank.OWNER)) {
			String message = event.getMessage();
			
			try {
				String urlMessage = URLEncoder.encode(message, StandardCharsets.UTF_8.name());
				String filterUrl = URL + urlMessage;
				
				String response = UtilNet.read(filterUrl, 300);
				if (response != null) {
					event.setMessage(response);
				}
				
			} catch (UnsupportedEncodingException ex) {
				// This should never happen
				ex.printStackTrace();
			}
		}
	}
	
	public void setSilenced(boolean silenced) {
		this.silenced = silenced;
	}
	
	public void silence(long silencedMillis) {
		this.silencedUntil = System.currentTimeMillis() + silencedMillis;
	}
	
	public void setSlow(long slowDelay) {
		this.slowDelay = slowDelay;
		this.slow = true;
	}
	
	public boolean isSilenced() {
		return silenced;
	}
	
	public long getSilencedUntil() {
		return silencedUntil;
	}
	
	public boolean isSlow() {
		return slow;
	}
	
	public long getSlowDelay() {
		return slowDelay;
	}
	
	public boolean isFilterEnabled() {
		return filter;
	}
	
	public void setFilterEnabled(boolean enabled) {
		this.filter = enabled;
	}
	
	
	
	
	
}
