package com.lebroncraft.core.chat.command;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.chat.ChatManager;
import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.rank.Rank;


public class CommandChat
		extends CommandModule<ChatManager> {
		
	public CommandChat(ChatManager chatManager) {
		super(chatManager, Rank.ADMIN, "chat");
		
		addSubCommand(new CommandChatSlow(chatManager));
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
	
	
	}
	
}
