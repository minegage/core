package com.lebroncraft.core.combat.modify;


public class DamageMult
		extends DamageBase {
		
	public DamageMult(String reason, double value) {
		super(reason, value);
	}
	
	@Override
	public double modify(double damage) {
		return damage *= value;
	}
	
}
