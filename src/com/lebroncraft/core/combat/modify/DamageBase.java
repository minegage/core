package com.lebroncraft.core.combat.modify;


public abstract class DamageBase {
	
	protected String reason;
	protected double value;
	
	public DamageBase(String reason, double value) {
		this.reason = reason;
		this.value = value;
	}
	
	public String getReason() {
		return reason;
	}
	
	public abstract double modify(double damage);
	
}
