package com.lebroncraft.core.combat.modify;


public class DamageAdd
		extends DamageBase {
		
	public DamageAdd(String reason, double value) {
		super(reason, value);
	}
	
	@Override
	public double modify(double damage) {
		return damage += value;
	}
	
}
