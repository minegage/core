package com.lebroncraft.core.combat;


import java.util.Map.Entry;
import java.util.WeakHashMap;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.combat.modify.DamageBase;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.core.event.CustomDeathEvent;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;


/**
 *
 */
public class CombatManager
		extends PluginModule {
		
	private WeakHashMap<LivingEntity, DamageHistory> trackers = new WeakHashMap<>();
	private long expireMillis = 30000L;
	private DeathMessenger deathMessenger;
	private boolean assists = true;
	
	public CombatManager(JavaPlugin plugin) {
		super("Combat Manager", plugin);
		this.deathMessenger = new DeathMessenger(this);
	}
	
	@EventHandler
	public void onTick(TickEvent event) {
		if (event.isNot(Tick.SEC_1)) {
			return;
		}
		
		for (DamageHistory damageHistory : trackers.values()) {
			damageHistory.expireOld();
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void customDamage(EntityDamageEvent event) {
		if (event.isCancelled()) {
			return;
		}
		
		Entity hitEntity = event.getEntity();
		
		if (!( hitEntity instanceof LivingEntity )) {
			return;
		}
		
		LivingEntity entity = (LivingEntity) hitEntity;
		
		CombatEvent combat = new CombatEvent(event);
		UtilEvent.call(combat);
		
		if (combat.isCancelled()) {
			event.setCancelled(true);
		} else {
			// Save the damage event information
			CombatDamage damage = new CombatDamage(combat);
			DamageHistory history = trackers.get(entity);
			if (history == null) {
				history = new DamageHistory(expireMillis);
				trackers.put(entity, history);
			}
			
			history.track(damage);
			
			// Remove the damage modifiers
			for (DamageModifier mod : DamageModifier.values()) {
				if (event.isApplicable(mod)) {
					event.setDamage(mod, 0.0);
				}
			}
			
			// Set the new damage modifiers
			for (Entry<DamageModifier, Double> source : combat.getVanillaMods()
					.entrySet()) {
				DamageModifier mod = source.getKey();
				Double val = source.getValue();
				event.setDamage(mod, val);
				
			}
			
			// Modify the base damage with the custom modifiers
			double baseDamage = event.getDamage(DamageModifier.BASE);
			for (DamageBase mod : combat.getModifiers()) {
				baseDamage = mod.modify(baseDamage);
			}
			
			event.setDamage(DamageModifier.BASE, baseDamage);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void preDeath(EntityDamageEvent event) {
		if (event.isCancelled()) {
			return;
		}
		
		Entity hitEntity = event.getEntity();
		
		if (!( hitEntity instanceof LivingEntity )) {
			return;
		}
		
		LivingEntity entity = (LivingEntity) hitEntity;
		
		double finalDamage = event.getFinalDamage();
		double health = entity.getHealth();
		
		if (health - finalDamage <= 0.0) {
			// Set the last damage cause prematurely so that it can be used in death processing
			entity.setLastDamageCause(event);
			
			// Remove the damage history
			DamageHistory damageHistory = trackers.remove(entity);
			
			CustomDeathEvent customDeathEvent = new CustomDeathEvent(event, damageHistory);
			UtilEvent.call(customDeathEvent);
			
			if (this.assists) {
				for (Entry<OfflinePlayer, Double> assists : customDeathEvent.getAssists()
						.entrySet()) {
						
					OfflinePlayer offPlayer = assists.getKey();
					Double damage = assists.getValue();
					
					KillAssistEvent assistEvent = new KillAssistEvent(offPlayer, damage, customDeathEvent.getDamageHistory()
							.getDamage());
							
					UtilEvent.call(assistEvent);
				}
			}
			
			// Prevent the respawn screen from showing
			if (entity instanceof Player) {
				Player player = (Player) entity;
				
				/* Fake the player death */
				PlayerDeathEvent deathEvent = new PlayerDeathEvent(player, customDeathEvent.getDrops(), 0, null);
				UtilEvent.call(deathEvent);
				
				/* Fake the hurt sound, as it won't normally be sent */
				UtilSound.playPhysical(player.getLocation(), Sound.HURT_FLESH, 1F, 1F);
				
				Location respawnLoc = customDeathEvent.getRespawnLocation();
				
				if (respawnLoc != null) {
					player.teleport(respawnLoc);
				}
				
				/* After all death related processing is done, prevent player death */
				event.setDamage(0.0);
			}
		}
	}
	
	public WeakHashMap<LivingEntity, DamageHistory> getHistory() {
		return trackers;
	}
	
	public long getExpireMillis() {
		return expireMillis;
	}
	
	public void setExpireMillis(long expireMillis) {
		this.expireMillis = expireMillis;
	}
	
	public DeathMessenger getDeathMessenger() {
		return deathMessenger;
	}
	
	public boolean isAssistsEnabled() {
		return assists;
	}
	
	public void setAssistsEnabled(boolean assistsEnabled) {
		this.assists = assistsEnabled;
	}
	
	
	
}
