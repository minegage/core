package com.lebroncraft.core.common.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.v1_8_R2.CraftServer;

import com.lebroncraft.core.log.L;

public class UtilBukkit {
	
	private static Field craftServerConfig;
	
	/**
	 * @return The FileConfiguration object stored in the CraftServer class
	 */
	public static FileConfiguration getBukkitConfig() {
		if (craftServerConfig == null) {
			try {
				craftServerConfig = CraftServer.class.getDeclaredField("configuration");
				craftServerConfig.setAccessible(true);
			} catch (NoSuchFieldException | SecurityException ex) {
				ex.printStackTrace();
			}
		}
		
		try {
			CraftServer server = (CraftServer) Bukkit.getServer();
			return (FileConfiguration) craftServerConfig.get(server);
		} catch (IllegalArgumentException | IllegalAccessException ex) {
			ex.printStackTrace();
			return null;
		}
	}
	
	public static void saveBukkitConfig() {
		try {
			getBukkitConfig().save(getBukkitConfigFile());
		} catch (IOException ex) {
			L.severe("Unable to save Bukkit config; an IOException occurred");
			ex.printStackTrace();
		}
	}
	
	public static File getBukkitConfigFile() {
		return new File(Bukkit.getWorldContainer(), "bukkit.yml");
	}
	
}
