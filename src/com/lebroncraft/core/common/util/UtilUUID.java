package com.lebroncraft.core.common.util;

import java.util.UUID;

import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Entity;

public class UtilUUID {
	
	public static Entity getEntity(UUID uid, Server server) {
		for (World world : server.getWorlds()) {
			Entity entity = getEntity(uid, world);
			if (entity != null) {
				return entity;
			}
		}
		return null;
	}
	
	public static Entity getEntity(UUID uid, World world) {
		for (Entity entity : world.getEntities()) {
			if (entity.getUniqueId().equals(uid)) {
				return entity;
			}
		}
		return null;
	}
	
}
