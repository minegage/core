package com.lebroncraft.core.common.util;



import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_8_R2.EntityHuman;
import net.minecraft.server.v1_8_R2.EntityPlayer;
import net.minecraft.server.v1_8_R2.Packet;
import net.minecraft.server.v1_8_R2.PlayerAbilities;


public class UtilPlayer {
	
	public static final float DEFAULT_FLY_SPEED = 0.05F;
	public static final float DEFAULT_WALK_SPEED = 0.1F;
	
	public static void setCollides(Player player, boolean collides) {
		player.spigot()
				.setCollidesWithEntities(collides);
	}
	
	public static boolean getCollides(Player player) {
		return player.spigot()
				.getCollidesWithEntities();
	}
	
	public static void setRawFlySpeed(Player player, float speed) {
		EntityHuman nmsHuman = getNmsHuman(player);
		PlayerAbilities abilities = nmsHuman.abilities;
		
		if (abilities.flySpeed != speed) {
			abilities.flySpeed = speed;
			nmsHuman.updateAbilities();
		}
	}
	
	public static void setRawWalkSpeed(Player player, float speed) {
		EntityHuman nmsHuman = getNmsHuman(player);
		PlayerAbilities abilities = nmsHuman.abilities;
		
		if (abilities.walkSpeed != speed) {
			abilities.walkSpeed = speed;
			nmsHuman.updateAbilities();
		}
	}
	
	public static void resetSpeed(Player player) {
		setRawFlySpeed(player, DEFAULT_FLY_SPEED);
		setRawWalkSpeed(player, DEFAULT_WALK_SPEED);
	}
	
	public static void resetInv(Player player) {
		UtilInv.clear(player.getInventory());
	}
	
	public static void resetProperties(Player player) {
		player.setSprinting(false);
		player.setLevel(0);
		player.setExp(0F);
		
		UtilEntity.resetHealth(player);
		resetSpeed(player);
	}
	
	public static void reset(Player player) {
		player.eject();
		player.setFlying(false);
		player.setAllowFlight(false);
		player.setGameMode(Bukkit.getDefaultGameMode());
		
		getNmsPlayer(player).reset();
		resetInv(player);
		resetProperties(player);
	}
	
	public static void setHunger(Player player, int foodLevel, float saturation, float exhaustion) {
		player.setFoodLevel(foodLevel);
		player.setSaturation(saturation);
		player.setExhaustion(exhaustion);
	}
	
	public static void resetHunger(Player player) {
		setHunger(player, 20, 5F, 0F);
	}
	
	public static EntityPlayer getNmsPlayer(Player player) {
		return ( (CraftPlayer) player ).getHandle();
	}
	
	public static EntityHuman getNmsHuman(Player player) {
		return ( (CraftPlayer) player ).getHandle();
	}
	
	public static void sendPacket(Player player, Packet<?> packet) {
		getNmsPlayer(player).playerConnection.sendPacket(packet);
	}
	
	public static boolean isHolding(Player player, ItemStack item) {
		ItemStack holding = player.getItemInHand();
		if (holding == null) {
			return false;
		}
		
		return holding.equals(item);
	}
	
	public static boolean isHolding(Player player, Material material) {
		return UtilItem.is(player.getItemInHand(), material);
	}
	
	public static boolean isHolding(Player player, Material material, byte data) {
		return UtilItem.is(player.getItemInHand(), material, data);
	}
	
}
