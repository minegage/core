package com.lebroncraft.core.common.util;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerListHeaderFooter;
import com.comphenix.packetwrapper.WrapperPlayServerTitle;
import com.comphenix.protocol.wrappers.EnumWrappers.TitleAction;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;

import net.minecraft.server.v1_8_R2.IChatBaseComponent;
import net.minecraft.server.v1_8_R2.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R2.PacketPlayOutChat;


public class UtilUI {
	
	public static WrapperPlayServerTitle createTitle(TitleAction action) {
		WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
		packet.setAction(action);
		return packet;
	}
	
	public static void sendTimes(Player player, int inTicks, int stayTicks, int outTicks) {
		WrapperPlayServerTitle packet = createTitle(TitleAction.TIMES);
		
		packet.setFadeIn(inTicks);
		packet.setStay(stayTicks);
		packet.setFadeOut(outTicks);
		
		packet.sendPacket(player);
	}
	
	public static void sendTitle(Player player, String text) {
		WrapperPlayServerTitle packet = createTitle(TitleAction.TITLE);
		packet.setTitle(WrappedChatComponent.fromText(text));
		packet.sendPacket(player);
	}
	
	public static void sendSubtitle(Player player, String text) {
		WrapperPlayServerTitle packet = createTitle(TitleAction.SUBTITLE);
		packet.setTitle(WrappedChatComponent.fromText(text));
		packet.sendPacket(player);
	}
	
	public static void sendTitle(Player player, String text, int inTicks, int stayTicks, int outTicks) {
		sendTimes(player, inTicks, stayTicks, outTicks);
		sendTitle(player, text);
	}
	
	public static void sendSubtitle(Player player, String text, int inTicks, int stayTicks, int outTicks) {
		sendTimes(player, inTicks, stayTicks, outTicks);
		sendSubtitle(player, text);
	}
	
	public static void sendTitles(Player player, String title, String subtitle) {
		sendTitle(player, title);
		sendSubtitle(player, subtitle);
	}
	
	public static void sendTitles(Player player, String title, String subtitle, int inTicks, int stayTicks, int outTicks) {
		sendTimes(player, inTicks, stayTicks, outTicks);
		sendTitle(player, title);
		sendSubtitle(player, subtitle);
	}
	
	
	public static void clearTitle(Player player) {
		WrapperPlayServerTitle packet = new WrapperPlayServerTitle();
		packet.setAction(TitleAction.CLEAR);
		packet.sendPacket(player);
	}
	
	public static void sendTabText(Player player, String header, String footer) {
		WrapperPlayServerPlayerListHeaderFooter packet = new WrapperPlayServerPlayerListHeaderFooter();
		
		WrappedChatComponent headerComponent = WrappedChatComponent.fromText(header);
		WrappedChatComponent footerComponent = WrappedChatComponent.fromText(footer);
		
		packet.setHeader(headerComponent);
		packet.setFooter(footerComponent);
		
		packet.sendPacket(player);
	}
	
	public static void sendActionBar(Player player, String message) {
		IChatBaseComponent textWrap = ChatSerializer.a("{\"text\":\"" + message + "\"}");
		PacketPlayOutChat barPacket = new PacketPlayOutChat(textWrap, (byte) 2);
		UtilPlayer.sendPacket(player, barPacket);
	}
	
	public static String getServerDisplay(String server) {
		return C.fDash(C.cGreen + C.cBold + server, C.cBold + "Lebroncraft");
	}
	
	public static void assignRankTeams(Board board) {
		RankManager rankManager = RankManager.instance;
		Scoreboard scoreboard = board.getBoard();
		
		for (Player other : UtilServer.players()) {
			Rank otherRank = rankManager.getRank(other);
			
			Team team = scoreboard.getTeam(otherRank.getTeamName());
			if (team != null) {
				team.addPlayer(other);
			}
			
			other.setDisplayName(otherRank.getChatPrefix() + other.getName() + C.cReset);
		}
	}
	
	public static void createRankTeams(Board board) {
		for (Rank rank : Rank.values()) {
			Team team = board.getBoard()
					.registerNewTeam(rank.getTeamName());
			team.setPrefix(rank.getTeamPrefix());
		}
	}
	
	public static String getTimer(int seconds) {
		int min = Math.max(seconds / 60, 0);
		int sec = Math.max(seconds % 60, 0);
		
		int minLen = String.valueOf(min)
				.length();
		int secLen = String.valueOf(sec)
				.length();
				
		return ( minLen < 2 ) ? ( secLen < 2 ) ? ( "0" + min + ":" + "0" + sec ) : ( "0" + min + ":" + sec )
				: ( secLen < 2 ) ? ( min + ":0" + sec ) : ( min + ":" + sec );
	}
	
	public static Scoreboard getMainScoreboard() {
		return UtilUI.getBoardManager()
				.getMainScoreboard();
	}
	
	public static Scoreboard getNewScoreboard() {
		return UtilUI.getBoardManager()
				.getNewScoreboard();
	}
	
	public static ScoreboardManager getBoardManager() {
		return Bukkit.getScoreboardManager();
	}
	
}
