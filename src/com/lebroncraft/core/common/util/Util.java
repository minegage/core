package com.lebroncraft.core.common.util;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;


public class Util {
	
	public static void messageOperators(String italics, String message) {
		String str = ChatColor.GRAY + "" + ChatColor.ITALIC + "[" + italics + ": " + message + "]";
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (player.isOp()) {
				player.sendMessage(str);
			}
		}
	}
	
	/**
	 * Creates a string from all elements of the specified array
	 * 
	 * @param array
	 *        The array to join together
	 * @param spacer
	 *        The string separating each element of the array in the returning string
	 * @return The combined string
	 */
	public static <T> String joinArray(T[] array, String spacer) {
		return joinArray(array, spacer, 0, array.length - 1);
	}
	
	/**
	 * Creates a string from a range of elements of the specified array
	 * 
	 * @param spacer
	 *        The String separating each element of the array in the returning string
	 * @param startingIndex
	 *        The minimum index to be included
	 * @param finalIndex
	 *        The maximum index to be included
	 * @return The combined string, containing all elements of the array
	 */
	public static <T> String joinArray(T[] array, String spacer, int startingIndex, int finalIndex) {
		if (array.length == 0 || array == null) {
			return "";
		}
		
		String ret = "";
		for (int i = startingIndex; i <= finalIndex; i++) {
			ret = ret + spacer + array[i];
		}
		return ret.replaceFirst(spacer, "");
	}
	
	public static <T> String joinArray(T[] array, String delim, int startingIndex) {
		return joinArray(array, delim, startingIndex, array.length - 1);
	}
	
	
	
	public static <T> String joinList(List<T> list, String delim, int startingIndex, int finalIndex) {
		String ret = "";
		for (int i = startingIndex; i <= finalIndex; i++) {
			ret = ret + delim + list.get(i);
		}
		
		ret = UtilString.removeFirst(ret, delim);
		
		return ret;
	}
	
	public static <T> String joinList(List<T> list, String delim, int startingIndex) {
		return joinList(list, delim, startingIndex, list.size() - 1);
	}
	
	public static <T> String joinList(List<T> list, String delim) {
		return joinList(list, delim, 0);
	}
	
	public static <T> String joinList(List<T> e, String delim, Function<? super T, String> stringMap) {
		List<String> strings = e.stream()
				.map(stringMap)
				.collect(Collectors.toList());
		String joined = joinList(strings, delim);
		return joined;
	}
	
	public static <T> List<T> split(String string, String delim, Function<String, ? extends T> map) {
		return new ArrayList<>(UtilString.split(string, delim)).stream()
				.map(map)
				.collect(Collectors.toList());
	}
	
	public static String[] trimElements(String[] array, Integer... elements) {
		return trimElement(array, new HashSet<>(Arrays.asList(elements)));
	}
	
	public static String[] trimElement(String[] array, Set<Integer> elements) {
		// Check range
		for (Integer i : elements) {
			if (i < 0 || i > elements.size()) {
				throw new IllegalArgumentException("Element " + i + " out of range");
			}
		}
		
		int newIndex = 0;
		String[] ret = new String[elements.size()];
		
		for (int oldIndex = 0; oldIndex < array.length; oldIndex++) {
			
			// Skip element if it should be trimmed
			if (!elements.contains(oldIndex)) {
				ret[newIndex] = array[oldIndex];
				newIndex += 1;
			}
		}
		
		return ret;
	}
	
	@Deprecated
	public static String getFlags(String[] args) {
		String ret = "";
		
		for (String s : args) {
			if (s.startsWith("-")) {
				char flag = s.replaceFirst("-", "")
						.toLowerCase()
						.charAt(0);
				ret = ret + flag + " ";
			}
		}
		
		return ret.trim();
	}
	
	
	public static Object[] getSection(Object[] toSplit, int startIndex, int finalIndex) {
		if (finalIndex < startIndex) {
			int hold = finalIndex;
			finalIndex = startIndex;
			startIndex = hold;
		}
		
		Object[] split = new Object[finalIndex - startIndex + 1];
		for (int i = startIndex; i <= finalIndex; i++) {
			split[i - startIndex] = toSplit[i];
		}
		
		return split;
	}
	
	/**
	 * Returns a list of integers under n chosen from the specified list
	 */
	public static List<Integer> getIntegersUnder(int n, List<Integer> list) {
		List<Integer> toRemove = new ArrayList<Integer>();
		
		for (int i : list) {
			if (i > n) {
				toRemove.add(i);
			}
		}
		
		list.removeAll(toRemove);
		
		return list;
	}
	
	public static int[] getIntsUnder(int n, int[] ints) {
		int[] under = new int[ints.length];
		int index = 0;
		for (int i : ints)
			if (i < n)
				under[index++] = i;
				
		int[] ret = new int[index];
		int index2 = 0;
		for (int i = 0; i < index; i++)
			ret[index2++] = under[i];
			
		return ret;
	}
	
	public static List<String> mapToStringArrayList(Map<?, ?> map) {
		List<String> ret = new ArrayList<String>();
		
		for (Object key : map.keySet()) {
			Object value = map.get(key);
			
			if (key instanceof Map) {
				ret.addAll(mapToStringArrayList((Map<?, ?>) value));
			} else {
				ret.add("('" + key.toString() + "', '" + value.toString()
						.replace("=", ", ") + "')");
			}
		}
		return ret;
	}
	
	@Deprecated
	public static String vecToString(Vector vec, int precision) {
		return "(" + UtilMath.round(vec.getX(), precision) + ", " + UtilMath.round(vec.getY(), precision) + ", " + UtilMath.round(
				vec.getZ(), precision) + ")";
	}
	
	@Deprecated
	public static String locToString(Location loc, int precision) {
		return vecToString(loc.toVector(), precision);
	}
	
	public static int getLowestAvailable(List<Integer> list, int min) {
		int lowestAvailable = min;
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) == lowestAvailable) {
				lowestAvailable += 1;
				i = 0;
			}
		}
		return lowestAvailable;
	}
	
	@Deprecated
	public static String secondsDisplay(int seconds) {
		int min = seconds / 60;
		int sec = seconds - ( min * 60 );
		
		return String.valueOf(min)
				.length() < 2 ? String.valueOf(sec)
						.length() < 2 ? "0" + min + ":" + "0" + sec : "0" + min + ":" + sec : String.valueOf(sec)
								.length() < 2 ? min + ":0" + sec : min + ":" + sec;
	}
	
	@Deprecated
	public static String wordToCamelCase(String word) {
		word = word.toLowerCase();
		Character capital = Character.toUpperCase(word.charAt(0));
		word = word.replaceFirst(String.valueOf(Character.toLowerCase(capital)), String.valueOf(capital));
		return word;
	}
	
	@Deprecated
	public static String sentenceToCamelCase(String sentence) {
		String ret = "";
		String[] split = sentence.split(" ");
		for (String s : split) {
			ret = ret + " " + wordToCamelCase(s);
		}
		return ret.trim();
	}
	
	/**
	 * Creates a virtual number line based on the sector end points, picks a random between the max
	 * and min values on the number line, and picks the sector that this number lies in. Used for
	 * probability selection.
	 * 
	 * @see com.lebroncraft.minigamelib.item.containerfiller.ContainerFiller for example use
	 * 		
	 * @param decimalPlaces
	 *        The number of decimal places that the method should use
	 * @param sectorEndPoints
	 *        The ordered list of points indicating the end points of each sector
	 * @return The randomly chosen sector
	 */
	public static int getRandSectorBySectorEndPoints(double... sectorEndPoints) {
		double min = sectorEndPoints[0];
		double max = sectorEndPoints[0];
		
		for (double dub : sectorEndPoints) {
			if (dub < min) {
				min = dub;
			}
			if (dub > max) {
				max = dub;
			}
		}
		
		double rand = Rand.rDouble(min, max);
		
		for (int i = 0; i < sectorEndPoints.length; i++) {
			double dub = sectorEndPoints[i];
			
			if (rand <= dub) {
				return i;
			}
		}
		
		// Should not be possible unless method is not used correctly
		return -1;
	}
	
	public static int getRandSectorBySectorSizes(double... sectorSizes) {
		double[] sectorEndPoints = new double[sectorSizes.length];
		
		double add = 0;
		for (int i = 0; i < sectorSizes.length; i++) {
			add += sectorSizes[i];
			sectorEndPoints[i] = add;
		}
		
		return getRandSectorBySectorEndPoints(sectorEndPoints);
	}
	
	public static List<Location> getLocsSqrRadius(Location midloc, int radius) {
		List<Location> locs = new ArrayList<Location>();
		
		int lx = midloc.getBlockX(), ly = midloc.getBlockY(), lz = midloc.getBlockZ();
		
		for (int x = lx - radius; x < lx + radius; x++)
			for (int z = lz - radius; z < lz + radius; z++)
				for (int y = ly - radius; y < lz + radius; y++)
					locs.add(new Location(midloc.getWorld(), x, y, z));
					
		return locs;
	}
	
	public static String safeToString(Object o) {
		String ret = "(null)";
		if (o != null)
			ret = o.toString();
		return ret;
	}
	
	private static int idCount = 0;
	
	public static int nextID() {
		return idCount++;
	}
	
}
