package com.lebroncraft.core.common;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.bukkit.Location;
import org.bukkit.World;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.common.util.UtilString;


public class DataFile {
	
	public static final String DELIM_INFO = "|";
	public static final String DELIM_DESC = ",";
	public static final String DELIM_LINE = ":";
	
	protected File file;
	
	private SafeMap<String, String> information = new SafeMap<>();
	
	public DataFile(File file) {
		this.file = file;
	}
	
	public void loadFile() throws IOException {
		file.createNewFile();
		
		List<String> lines = FileUtils.readLines(file);
		
		for (String line : lines) {
			if (line.length() == 0) {
				continue;
			}
			
			String desc = "";
			String info = "";
			
			String[] lineSplit = line.split(DELIM_LINE, 2);
			
			desc = lineSplit[0];
			if (lineSplit.length > 1) {
				info = lineSplit[1];
			}
			
			information.put(desc, info);
		}
	}
	
	public void saveFile() throws IOException {
		List<String> lines = Lists.newArrayList();
		
		for (Entry<String, String> entry : information.entrySet()) {
			String desc = entry.getKey();
			String info = entry.getValue();
			
			String line = desc;
			if (info.length() > 0) {
				line += DELIM_LINE + info;
			}
			
			lines.add(line);
		}
		
		FileUtils.writeLines(file, lines);
	}
	
	public Set<Entry<String, String>> getEntries() {
		return information.entrySet();
	}
	
	/**
	 * @return An Entry Set containing all lines which contains the specified description
	 */
	public Set<Entry<String, String>> getEntries(String descContains) {
		Set<Entry<String, String>> entries = Sets.newHashSet();
		
		for (Entry<String, String> entry : getEntries()) {
			String desc = entry.getKey();
			
			if (desc.contains(descContains)) {
				entries.add(entry);
			}
		}
		
		return entries;
	}
	
	public String desc(String... desc) {
		return Util.joinArray(desc, DELIM_DESC);
	}
	
	public String desc(List<String> desc) {
		return Util.joinList(desc, DELIM_DESC);
	}
	
	public String info(String... info) {
		return Util.joinArray(info, DELIM_INFO);
	}
	
	public String info(List<String> info) {
		return Util.joinList(info, DELIM_INFO);
	}
	
	public List<String> info(String info) {
		return Lists.newArrayList(info.split(DELIM_INFO));
	}
	
	public List<String> desc(String desc) {
		return Lists.newArrayList(desc.split(DELIM_DESC));
	}
	
	public String read(String desc) {
		return information.getOrDefault(desc, null);
	}
	
	public String read(String... desc) {
		return read(desc(desc));
	}
	
	public String delete(String desc) {
		return information.remove(desc);
	}
	
	public void write(String desc, Object info) {
		information.put(desc, info.toString());
	}
	
	public void write(String desc, Object... info) {
		String joined = Util.joinArray(info, DELIM_INFO);
		write(desc, joined);
	}
	
	public List<String> toStrings(String str, String delim) {
		List<String> split = UtilString.split(str, delim);
		return split;
	}
	
	public List<String> fromInfo(String info) {
		return toStrings(info, DELIM_INFO);
	}
	
	public List<String> fromDesc(String desc) {
		return toStrings(desc, DELIM_DESC);
	}
	
	private <T> List<T> split(String info, Function<String, ? extends T> map) {
		return Util.split(info, DELIM_INFO, map);
	}
	
	public List<Location> toLocations(String info, World world) {
		return split(info, str -> UtilPos.deserializeLocation(str, world));
	}
	
	public List<Location> toPositions(String info, World world) {
		return split(info, str -> UtilPos.deserializeLocation(str, world));
	}
	
	public List<Double> toDoubles(String info) {
		return split(info, str -> Double.parseDouble(str));
	}
	
	public List<Integer> toInts(String info) {
		return split(info, str -> Integer.parseInt(str));
	}
	
	public Location toLocation(String info, World world) {
		return UtilPos.deserializeLocation(info, world);
	}
	
	public Location toPosition(String info, World world) {
		return UtilPos.deserializePosition(info, world);
	}
	
	public Double toDouble(String info) {
		return Double.parseDouble(info);
	}
	
	public Integer toInt(String info) {
		return Integer.parseInt(info);
	}
	
	private <T> String join(List<T> list, Function<? super T, String> map) {
		return Util.joinList(list, DELIM_INFO, map);
	}
	
	public String fromLocations(List<Location> locations) {
		return join(locations, loc -> UtilPos.serializeLocation(loc));
	}
	
	public String fromPositions(List<Location> positions) {
		return join(positions, loc -> UtilPos.serializePosition(loc));
	}
	
	public String fromStrings(List<String> strings) {
		return Util.joinList(strings, DELIM_INFO);
	}
	
	public String fromDoubles(List<Double> doubles) {
		return Util.joinList(doubles, DELIM_INFO, dub -> Double.toString(dub));
	}
	
	public String fromInts(List<Integer> ints) {
		return Util.joinList(ints, DELIM_INFO, i -> Integer.toString(i));
	}
	
	public String fromLocation(Location location) {
		return UtilPos.serializeLocation(location);
	}
	
	public String fromPosition(Location position) {
		return UtilPos.serializePosition(position);
	}
	
	public String fromDouble(Double n) {
		return Double.toString(n);
	}
	
	public String fromInteger(Integer n) {
		return Integer.toString(n);
	}
	
}
