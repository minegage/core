package com.lebroncraft.core.common;


import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilRegex;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;


public class C {
	
	public static final String SIGN = ChatColor.COLOR_CHAR + "";
	
	
	public static String translate(String string) {
		return ChatColor.translateAlternateColorCodes('&', string);
	}
	
	public static String strip(String string) {
		return UtilRegex.strip(string);
	}
	
	/* Colors */
	public static final String cAqua = ChatColor.AQUA.toString();
	public static final String cBlack = ChatColor.BLACK.toString();
	public static final String cBlue = ChatColor.BLUE.toString();
	public static final String cAquaD = ChatColor.DARK_AQUA.toString();
	public static final String cBlueD = ChatColor.DARK_BLUE.toString();
	public static final String cGrayD = ChatColor.DARK_GRAY.toString();
	public static final String cGreenD = ChatColor.DARK_GREEN.toString();
	public static final String cPurple = ChatColor.DARK_PURPLE.toString();
	public static final String cRedD = ChatColor.DARK_RED.toString();
	public static final String cGold = ChatColor.GOLD.toString();
	public static final String cGray = ChatColor.GRAY.toString();
	public static final String cGreen = ChatColor.GREEN.toString();
	public static final String cPink = ChatColor.LIGHT_PURPLE.toString();
	public static final String cRed = ChatColor.RED.toString();
	public static final String cWhite = ChatColor.WHITE.toString();
	public static final String cYellow = ChatColor.YELLOW.toString();
	
	/* Color formatting */
	
	public static final String cBold = ChatColor.BOLD.toString();
	public static final String cItalics = ChatColor.ITALIC.toString();
	public static final String cScram = ChatColor.MAGIC.toString();
	public static final String cReset = ChatColor.RESET.toString();
	public static final String cStrike = ChatColor.STRIKETHROUGH.toString();
	public static final String cLine = ChatColor.UNDERLINE.toString();
	
	/* Styling */
	
	
	public static final String sDash = "\u2014";
	
	public static final String sHead = C.cGold + C.cBold;
	public static final String sBody = C.cGray;
	
	public static final String sOut = C.cYellow;
	public static final String sOut2 = C.cPink;
	
	public static final String shHead = C.cGold;
	public static final String shDesc = C.cWhite;
	
	public static final String sErrHead = C.cRed + C.cBold;
	public static final String sErrBody = C.cGold;
	
	public static final String sWarnHead = C.cRed + C.cBold;
	public static final String sWarnBody = C.cGold;
	
	public static final String sAnnHead = C.cWhite + C.cBold;
	public static final String sAnnDesc = C.cGreen + C.cBold;
	
	public static final String sNotify = C.cGray;
	
	/* Inventory styling */
	public static final String iMain = C.cWhite;
	public static final String iOut = C.cGreen;
	public static final String iOut2 = C.cYellow;
	
	/* Rank styling */
	
	public static final String rMember = C.cGray;
	public static final String rPro = C.cYellow;
	public static final String rAce = C.cGreen;
	public static final String rMvp = C.cPink;
	public static final String rYoutube1 = C.cWhite;
	public static final String rYoutube2 = C.cRedD;
	public static final String rYoutubeName = C.cRed;
	public static final String rBuilder = C.cGold;
	public static final String rMod = C.cAqua;
	public static final String rAdmin = C.cRed;
	public static final String rDev = C.cBlue;
	public static final String rOwner = C.cGreenD;
	
	/* Tabs/indents */
	
	public static final String t1 = "  ";
	public static final String t2 = "    ";
	public static final String t3 = "      ";
	public static final String t4 = "        ";
	
	/* Formatting */
	
	public static String fElem(String elem) {
		return C.sOut + elem + C.sBody;
	}
	
	public static String fElem2(String elem) {
		return C.sOut2 + elem + C.sBody;
	}
	
	/**
	 * Includes spaces left/right of the dash
	 * 
	 * @return
	 */
	public static String fDash() {
		return C.cReset + " " + sDash + " ";
	}
	
	public static String fDash(String left, String right) {
		return left + fDash() + right;
	}
	
	public static String fGeneral(String head, String body) {
		return head + fDash() + body;
	}
	
	public static String fMain(String head, String body) {
		return fGeneral(sHead + head, sBody + body);
	}
	
	public static String fWarn(String head, String body) {
		return fGeneral(sWarnHead + C.cBold + head, sWarnBody + C.cBold + body);
	}
	
	public static String fErr(String head, String body) {
		return fGeneral(C.sErrHead + head, C.sErrBody + body);
	}
	
	public static String fOut(String out) {
		return C.sOut + out;
	}
	
	public static String fOut2(String out2) {
		return C.sOut2 + out2;
	}
	
	/* Player messaging */
	
	public static void pRaw(Player player, String message) {
		player.sendMessage(message);
	}
	
	public static void pGeneral(Player player, String head, String body) {
		pRaw(player, fGeneral(head, body));
	}
	
	public static void pMain(Player player, String head, String body) {
		pRaw(player, fMain(head, body));
	}
	
	/**
	 * Should be used like this: <code>{@link C#pErr(Exception, Player, "Unable to do X")}</code>
	 */
	public static void pErr(Exception ex, Player player, String result) {
		L.error(ex, result);
		pRaw(player, C.fErr("Error", result + "; A " + ex.getClass()
				.getSimpleName() + " occurred"));
	}
	
	public static void pWarn(Player player, String head, String message) {
		pRaw(player, C.fWarn(head, message));
	}
	
	/* Command help */
	public static void pHelp(Player player, String command, String desc) {
		pRaw(player, C.shHead + "/" + command + C.cReset + ": " + C.shDesc + desc);
	}
	
	public static void pHelp(Player player, String command) {
		pRaw(player, C.shHead + "/" + command);
	}
	
	
	/* Broadcasting */
	
	public static void bRaw(String message) {
		UtilServer.broadcast(message);
	}
	
	public static void bMain(String head, String message) {
		bRaw(fMain(head, message));
	}
	
	public static void bGeneral(String head, String message) {
		bRaw(fGeneral(head, message));
	}
	
	public static void bWarn(String head, String message) {
		bRaw(fWarn(head, message));
	}
	
	public static void bNotify(Rank rank, String head, String message) {
		bRank(rank, C.sNotify + head + C.fDash() + C.sNotify + message);
	}
	
	/* Broadcasts a message to all players with the specified permission */
	public static void bRank(Rank rank, String message) {
		for (Entry<Player, Rank> pEntry : RankManager.instance.ranks.entrySet()) {
			Rank pRank = pEntry.getValue();
			
			if (rank.includes(pRank)) {
				C.pRaw(pEntry.getKey(), message);
			}
		}
	}
	
	
	/* Rainbow list */
	
	private static List<String> rainbow;
	
	static {
		rainbow = Lists.newArrayList();
		rainbow.add(C.cRedD);
		rainbow.add(C.cRed);
		rainbow.add(C.cGold);
		rainbow.add(C.cYellow);
		rainbow.add(C.cGreen);
		rainbow.add(C.cGreenD);
		rainbow.add(C.cAqua);
		rainbow.add(C.cAquaD);
		rainbow.add(C.cBlue);
		rainbow.add(C.cBlueD);
		rainbow.add(C.cPurple);
		rainbow.add(C.cPink);
	}
	
	public static List<String> bow() {
		return rainbow;
	}
	
	public static List<String> rainbow(String start) {
		List<String> bow = bow();
		Iterator<String> bowIt = UtilJava.wrappedIterator(bow, bow.indexOf(start));
		
		return UtilJava.join(bowIt);
	}
	
}
