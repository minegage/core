package com.lebroncraft.core.common.token;


import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.World;


public class WorldToken {
	
	public UUID worldUID;
	public String worldName;
	
	public WorldToken(World world) {
		this.worldUID = world.getUID();
		this.worldName = world.getName();
	}
	
	public World getWorld() {
		return Bukkit.getWorld(worldUID);
	}
	
}
