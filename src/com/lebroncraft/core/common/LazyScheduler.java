package com.lebroncraft.core.common;


import java.util.Iterator;
import java.util.Map.Entry;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import com.lebroncraft.core.common.java.SafeMap;


/**
 * Facilitates use of the Bukkit scheduler - allows for naming of tasks instead of relying on task
 * ids.
 */
public class LazyScheduler
		implements Runnable {
		
	private Object asyncLock = new Object();
	
	private SafeMap<String, Integer> syncTasks = new SafeMap<>();
	private SafeMap<String, Integer> asyncTasks = new SafeMap<>();
	
	protected JavaPlugin plugin;
	
	public LazyScheduler(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	/**
	 * Schedules a sync delayed task. It is not cancellable through this class
	 * 
	 * @return The bukkit task id
	 */
	public int runSyncDelayed(long delayTicks, Runnable runnable) {
		return getScheduler().runTaskLater(plugin, runnable, delayTicks)
				.getTaskId();
	}
	
	public void runSyncDelayed(String name, long delayTicks, Runnable runnable) {
		if (syncTasks.containsKey(name)) {
			throw new IllegalStateException("Sync delayed task \"" + name + "\" is already scheduled");
		}
		
		Runnable wrap = new Runnable() {
			@Override
			public void run() {
				runnable.run();
				syncTasks.remove(name);
			}
		};
		
		int taskId = getScheduler().runTaskLater(plugin, wrap, delayTicks)
				.getTaskId();
		addSyncTask(name, taskId);
	}
	
	public void runSyncTimer(String name, long delayTicks, long intervalTicks, Runnable runnable) {
		if (syncTasks.containsKey(name)) {
			throw new IllegalStateException("Sync timer task \"" + name + "\" is already scheduled");
		}
		
		int taskId = getScheduler().runTaskTimer(plugin, runnable, delayTicks, intervalTicks)
				.getTaskId();
		addSyncTask(name, taskId);
	}
	
	/**
	 * Schedules an async delayed task. It is not cancellable through this class
	 * 
	 * @return The bukkit task id
	 */
	public int runAsyncDelayed(long delayTicks, Runnable runnable) {
		return getScheduler().runTaskLaterAsynchronously(plugin, runnable, delayTicks)
				.getTaskId();
	}
	
	public void runAsyncDelayed(String name, long delayTicks, Runnable runnable) {
		if (asyncTasks.containsKey(name)) {
			throw new IllegalStateException("Async delayed task \"" + name + "\" is already scheduled");
		}
		
		Runnable wrapper = new Runnable() {
			@Override
			public void run() {
				runnable.run();
				synchronized (asyncLock) {
					asyncTasks.remove(name);
				}
			}
		};
		
		int taskId = getScheduler().runTaskLaterAsynchronously(plugin, wrapper, delayTicks)
				.getTaskId();
		addAsyncTask(name, taskId);
	}
	
	public void runAsyncTimer(String name, long delayTicks, long intervalTicks, Runnable runnable) {
		if (asyncTasks.containsKey(name)) {
			throw new IllegalStateException("Async timer task \"" + name + "\" is already scheduled");
		}
		
		int taskId = getScheduler().runTaskTimerAsynchronously(plugin, runnable, delayTicks, intervalTicks)
				.getTaskId();
		addAsyncTask(name, taskId);
	}
	
	public void cancelSyncTask(String name) {
		int taskId = syncTasks.getOrDefault(name, -1);
		
		if (taskId != -1) {
			getScheduler().cancelTask(taskId);
			syncTasks.remove(name);
		}
	}
	
	public void cancelAsyncTask(String name) {
		synchronized (asyncLock) {
			int taskId = asyncTasks.getOrDefault(name, -1);
			
			if (taskId != -1) {
				getScheduler().cancelTask(taskId);
				asyncTasks.remove(name);
			}
		}
	}
	
	public void cancelAllTasks() {
		cancelAllSyncTasks();
		cancelAllAsyncTasks();
	}
	
	public void cancelAllSyncTasks() {
		Iterator<Entry<String, Integer>> syncIt = syncTasks.entrySet()
				.iterator();
		while (syncIt.hasNext()) {
			Entry<String, Integer> syncNext = syncIt.next();
			
			Integer taskId = syncNext.getValue();
			
			getScheduler().cancelTask(taskId);
			syncIt.remove();
		}
	}
	
	public void cancelAllAsyncTasks() {
		synchronized (asyncLock) {
			Iterator<Entry<String, Integer>> asyncIt = asyncTasks.entrySet()
					.iterator();
			while (asyncIt.hasNext()) {
				Entry<String, Integer> asyncNext = asyncIt.next();
				
				Integer taskId = asyncNext.getValue();
				
				getScheduler().cancelTask(taskId);
				asyncIt.remove();
			}
		}
	}
	
	public SafeMap<String, Integer> getSyncTasks() {
		return syncTasks;
	}
	
	public SafeMap<String, Integer> getAsyncTasks() {
		synchronized (asyncLock) {
			return asyncTasks;
		}
	}
	
	private void addSyncTask(String name, int id) {
		syncTasks.put(name, id);
	}
	
	private void addAsyncTask(String name, int id) {
		synchronized (asyncLock) {
			asyncTasks.put(name, id);
		}
	}
	
	protected BukkitScheduler getScheduler() {
		return plugin.getServer()
				.getScheduler();
	}
	
	@Override
	public void run() {
		// Optional override
	}
	
}
