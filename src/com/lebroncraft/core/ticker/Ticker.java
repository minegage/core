package com.lebroncraft.core.ticker;


import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.module.PluginModule;


public class Ticker
		extends PluginModule
		implements Runnable {
		
	private int taskId = -1;
	
	public Ticker(JavaPlugin plugin) {
		super("Ticker", plugin);
	}
	
	@Override
	public void onEnable() {
		taskId = getScheduler().runTaskTimer(plugin, this, 1L, 1L)
				.getTaskId();
	}
	
	@Override
	public void onDisable() {
		super.onDisable();
		getScheduler().cancelTask(taskId);
	}
	
	@Override
	public void run() {
		int currentTick = UtilServer.currentTick();
		
		for (Tick type : Tick.values()) {
			if (type.hasPassed(currentTick)) {
				type.last = currentTick;
				
				try {
					getPluginManager().callEvent(new TickEvent(type));
				} catch (Throwable ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	public static enum Tick {
		MIN_1(1200),
		SEC_30(600),
		SEC_20(400),
		SEC_15(300),
		SEC_10(200),
		SEC_5(100),
		SEC_2(40),
		SEC_1(20),
		TICK_10(10),
		TICK_5(5),
		TICK_2(2),
		TICK_1(1);
		
		protected int interval;
		protected int last;
		
		private Tick(int interval) {
			this.interval = interval;
			this.last = UtilServer.currentTick();
		}
		
		public boolean hasPassed(int currentTick) {
			return currentTick - last >= interval;
		}
	}
	
}
