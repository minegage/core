package com.lebroncraft.core.command.message;

import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.rank.Rank;

public class CommandMessage 
		extends CommandBase {
	
	private MessageManager messageManager;
	
	public CommandMessage(MessageManager messageManager) {
		super(Rank.MEMBER, "message", "msg", "m", "tell", "t", "whisper");
		this.messageManager = messageManager;
	}

	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() == 0) {
			C.pMain(player, "Msg", "Please specify a player");
			return;
		}
		
		String target = args.get(0);
		Player match = messageManager.getServer().getPlayer(target);
		
		if (match == null) {
			C.pMain(player, "Msg", "Player \"" + target + "\" not found");
			return;
		}
		
		if (args.size() == 1) {
			C.pMain(player, "Msg", "Please enter a message");
			return;
		}
		
		String message = Util.joinList(args, " ", 1, args.size() - 1);
		messageManager.message(player, match, message);
	}	
	
}
