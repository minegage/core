package com.lebroncraft.core.command.message;

import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.rank.Rank;

public class CommandSpy 
		extends CommandBase {

	private MessageManager messageManager;
	
	public CommandSpy(MessageManager messageManager) {
		super(Rank.MODERATOR, "spy");
		this.messageManager = messageManager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		messageManager.toggleSpy(player, true);
	}
	
}
