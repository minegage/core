package com.lebroncraft.core.command.message;

import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.rank.Rank;

public class CommandReply 
		extends CommandBase {
	
	private MessageManager messageManager;
	
	public CommandReply(MessageManager messageManager) {
		super(Rank.MEMBER, "reply", "r");
		this.messageManager = messageManager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() == 0) {
			C.pMain(player, "Msg", "Please specify a message");
			return;
		}
		
		String message = Util.joinList(args, " ");
		messageManager.reply(player, message);
	}
	
}
