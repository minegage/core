package com.lebroncraft.core.command.message;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.Note;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;


public class MessageManager
		extends PluginModule {
		
	private SafeMap<UUID, UUID> lastReceived = new SafeMap<>();
	private Set<UUID> spy = new HashSet<>();
	
	public MessageManager(JavaPlugin plugin) {
		super("Message Manager", plugin);
		
		registerCommand(new CommandMessage(this));
		registerCommand(new CommandReply(this));
		registerCommand(new CommandSpy(this));
	}
	
	@EventHandler
	public void removeSpy(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		UUID uid = player.getUniqueId();
		if (!RankManager.instance.getRank(player)
				.includes(Rank.MODERATOR)) {
			spy.remove(uid);
		} else {
			spy.add(uid);
		}
	}
	
	@EventHandler
	public void clearReference(PlayerQuitEvent event) {
		UUID uid = event.getPlayer()
				.getUniqueId();
		lastReceived.remove(uid);
		
		Iterator<Entry<UUID, UUID>> receivedIt = lastReceived.entrySet()
				.iterator();
		while (receivedIt.hasNext()) {
			if (receivedIt.next()
					.getValue()
					.equals(uid)) {
				receivedIt.remove();
			}
		}
	}
	
	public void message(Player from, Player to, String message) {
		message = C.cYellow + message;
		
		String fromName = from.getName();
		String toName = to.getName();
		
		C.pMain(from, "You > " + toName, message);
		C.pMain(to, fromName + " > You", message);
		
		for (int i = 0; i < 3; i++) {
			UtilSound.playLocal(to, Sound.NOTE_PIANO, 1F, Note.O3_D);
		}
		
		String spyMessage = fromName + " > " + toName + ": " + C.cReset + message;
		
		for (UUID uid : spy) {
			Player player = getServer().getPlayer(uid);
			if (player != null && !player.equals(from) && !player.equals(to)) {
				C.pMain(player, "Spy", spyMessage);
			}
		}
		
		lastReceived.put(to.getUniqueId(), from.getUniqueId());
	}
	
	public void reply(Player player, String message) {
		UUID uid = lastReceived.get(player.getUniqueId());
		
		if (uid == null) {
			C.pMain(player, "Msg", "You don't have anyone to reply to");
			return;
		}
		
		Player other = getServer().getPlayer(uid);
		
		if (other == null) {
			C.pMain(player, "Msg", "That player is not online");
			return;
		}
		
		message(player, other, message);
	}
	
	public void toggleSpy(Player player, boolean notify) {
		UUID uid = player.getUniqueId();
		
		String state = null;
		
		if (spy.contains(uid)) {
			spy.remove(uid);
			state = "disabled";
		} else {
			spy.add(uid);
			state = "enabled";
		}
		
		if (notify) {
			C.pMain(player, "Spy", "Spy mode " + C.sOut + state);
		}
	}
	
}
