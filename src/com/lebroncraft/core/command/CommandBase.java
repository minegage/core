package com.lebroncraft.core.command;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.entity.Player;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.data.DataParseException;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;


public abstract class CommandBase {
	
	public static final String HEADER = "System";
	
	protected Rank minRank;
	
	// List to preserve order; first alias is the name of the command
	protected List<String> aliases = new ArrayList<>();
	protected Set<CommandBase> subCommands = new HashSet<>();
	
	protected SafeMap<String, Data<?>> flagTypes = new SafeMap<>();
	
	public CommandBase(Rank minRank, String... names) {
		this.minRank = minRank;
		
		if (names == null || names.length == 0) {
			throw new IllegalArgumentException("Command must have at least one alias");
		}
		
		for (String alias : names) {
			if (alias == null || alias.equals("")) {
				throw new IllegalArgumentException("Command cannot have null or empty alias");
			} else {
				aliases.add(alias.toLowerCase());
			}
		}
	}
	
	public abstract void onCommand(Player player, List<String> args, Flags flags);
	
	// Full message needs to be passed in case of an error
	public void handleCommand(Player player, String message, List<String> args) {
		String sub = "";
		if (args.size() > 0) {
			sub = args.get(0);
		}
		
		// Recursive subcommand handling
		for (CommandBase command : subCommands) {
			if (command.getAliases()
					.contains(sub.toLowerCase())) {
					
				args.remove(0);
				command.handleCommand(player, message, args);
				return;
			}
		}
		
		if (!hasPermisison(player)) {
			C.pMain(player, HEADER, "That command requires rank " + getMinRank().getDisplayName() + C.sBody);
			return;
		}
		
		SafeMap<String, String> definedFlags = getFlags(args);
		
		// Check data types against all flags
		for (Entry<String, String> entry : definedFlags.entrySet()) {
			String flag = entry.getKey();
			String value = entry.getValue();
			
			Data<?> valueType = flagTypes.getOrDefault(flag, Data.STRING);
			
			try {
				valueType.parse(value);
			} catch (DataParseException ex) {
				C.pMain(player, "Error", "Invalid value \"" + value + "\" for flag \"" + flag + "\"");
				return;
			}
		}
		
		Flags flags = new Flags(definedFlags);
		
		try {
			onCommand(player, args, flags);
		} catch (Exception ex) {
			C.pMain(player, HEADER, "Something went wrong when running that command");
			L.error(ex, "Unable to process command \"" + message + "\" sent by " + player.getName());
		}
	}
	
	private SafeMap<String, String> getFlags(List<String> args) {
		SafeMap<String, String> ret = new SafeMap<>();
		
		Iterator<String> argsIt = args.iterator();
		
		while (argsIt.hasNext()) {
			String flag = argsIt.next();
			
			// Skip arguments which don't start with dashes, and skip arguments which are only
			// dashes
			if (!flag.startsWith("-") || flag.length() < 2) {
				continue;
			}
			
			// Remove the dash
			flag = flag.substring(1, flag.length());
			
			Data<?> type = flagTypes.get(flag);
			// Skip flags which aren't registered
			if (type == null) {
				continue;
			}
			
			String value = null;
			
			// Get the value, if any
			if (!type.equals(Data.NULL) && argsIt.hasNext()) {
				argsIt.remove();
				value = argsIt.next();
			}
			
			ret.put(flag, value);
			argsIt.remove();
		}
		
		return ret;
	}
	
	protected void addFlag(String flag, Data<?> argType) {
		flagTypes.put(flag, argType);
	}
	
	protected void addSubCommand(CommandBase commandBase) {
		subCommands.add(commandBase);
	}
	
	public Rank getMinRank() {
		return minRank;
	}
	
	public List<String> getAliases() {
		return aliases;
	}
	
	public String getName() {
		return aliases.get(0);
	}
	
	public CommandManager getCommandManager() {
		return CommandManager.instance;
	}
	
	public boolean hasPermisison(Player player) {
		return RankManager.instance.hasPermission(player, minRank);
	}
	
	
}
