package com.lebroncraft.core.command;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.lebroncraft.core.CorePlugin;
import com.lebroncraft.core.command.message.MessageManager;
import com.lebroncraft.core.command.misc.CommandColour;
import com.lebroncraft.core.command.misc.CommandItem;
import com.lebroncraft.core.command.misc.CommandSnap;
import com.lebroncraft.core.command.misc.CommandVanish;
import com.lebroncraft.core.command.speed.CommandSpeed;
import com.lebroncraft.core.command.world.CommandWorld;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.common.util.UtilRegex;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.mob.command.CommandMob;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.party.PartyManager;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.timer.Timer;


public final class CommandManager
		extends PluginModule {
		
	public static CommandManager instance;
	public static final String HEADER = "System";
	
	private Set<String> hidden = new HashSet<>();
	private Set<CommandBase> commands = new HashSet<>();
	
	protected MessageManager messageManager;
	protected PartyManager partyManager;
	
	public CommandManager(CorePlugin plugin) {
		super("Command Manager", plugin);
		CommandManager.instance = this;
		
		messageManager = new MessageManager(plugin);
		partyManager = new PartyManager(plugin);
		
		addCommand(new CommandMob());
		addCommand(new CommandWorld());
		addCommand(new CommandSpeed());
		addCommand(new CommandSnap());
		addCommand(new CommandItem());
		addCommand(new CommandColour());
		addCommand(new CommandVanish(plugin));
		
		hideCommand("me");
		hideCommand("minecraft:me");
		hideCommand("kill");
		hideCommand("minecraft:kill");
	}
	
	public void hideCommand(String command) {
		hidden.add(command);
	}
	
	public void addCommand(CommandBase commandBase) {
		commands.add(commandBase);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerCommand(PlayerCommandPreprocessEvent event) {
		L.info(event.getPlayer()
				.getName() + " sent command '" + event.getMessage() + "'");
				
		if (event.isCancelled()) {
			return;
		}
		
		Player player = event.getPlayer();
		
		if (!RankManager.instance.hasPermission(player, Rank.DEVELOPER)) {
			if (!Timer.instance.use(player, null, "Send Command", 500L, false)) {
				C.pMain(player, HEADER, "You can't send commands that fast");
				event.setCancelled(true);
				return;
			}
		}
		
		String eventMessage = event.getMessage();
		String message = eventMessage.substring(1, eventMessage.length());
		
		List<String> arguments = UtilRegex.matchArguments(message);
		
		if (arguments.size() == 0) {
			return;
		}
		
		String commandName = arguments.remove(0)
				.toLowerCase();
				
		if (hidden.contains(commandName) && !RankManager.instance.hasPermission(player, Rank.MODERATOR)) {
			UtilEvent.sendUnknownCommand(player);
			event.setCancelled(true);
			return;
		}
		
		for (CommandBase commandBase : commands) {
			if (commandBase.getAliases()
					.contains(commandName)) {
				commandBase.handleCommand(player, message, arguments);
				event.setCancelled(true);
				return;
			}
		}
		
		// Command not found
	}
	
	
}
