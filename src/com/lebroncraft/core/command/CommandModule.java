package com.lebroncraft.core.command;

import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.rank.Rank;

public abstract class CommandModule <T extends PluginModule>
		extends CommandBase {
	
	protected T plugin;
	
	public CommandModule(T plugin, Rank minRank, String... names) {
		super(minRank, names);
		this.plugin = plugin;
	}
	
	public T getPlugin() {
		return plugin;
	}
	
}
