package com.lebroncraft.core.command.speed;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.data.DataFloat;
import com.lebroncraft.core.common.util.UtilCommand;
import com.lebroncraft.core.common.util.UtilPlayer;
import com.lebroncraft.core.rank.Rank;


public class CommandSpeed
		extends CommandBase {
		
	public static final float MAX_SPEED = 10F;
	public static final float MIN_SPEED = 0F;
	
	
	public CommandSpeed() {
		super(Rank.MODERATOR, "speed");
		addFlag("all", Data.STRING);
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (UtilCommand.failIfTrue(args.size() == 0, player, "Speed", "Please specify a speed value, or reset")) {
			return;
		}
		
		String unparsed = args.get(0);
		
		String message = null;
		
		boolean fly = player.isFlying();
		boolean walk = !fly;
		
		if (unparsed.equalsIgnoreCase("reset")) {
			fly = true;
			walk = true;
			
			UtilPlayer.resetSpeed(player);
			
			message = "reset";
		} else {
			
			DataFloat speed = new DataFloat();
			if (UtilCommand.failedParse(speed, unparsed, player, "Speed", "Invalid speed value \"" + unparsed
					+ "\", must be a number or \"reset\"")) {
				return;
			}
			
			float userSpeed = speed.getData();
			
			if (userSpeed < MIN_SPEED || userSpeed > MAX_SPEED) {
				C.pMain(player, "Speed", "Value \"" + userSpeed + "\" out of range; must be equal to or between " + MIN_SPEED
						+ " and " + MAX_SPEED);
				return;
			}
			
			float defaultSpeed = ( fly ) ? UtilPlayer.DEFAULT_FLY_SPEED : UtilPlayer.DEFAULT_WALK_SPEED;
			float speedValue;
			
			if (userSpeed < 1f) {
				speedValue = defaultSpeed * userSpeed;
			} else {
				float ratio = ( ( userSpeed - 1 ) / 9 ) * ( 1F - defaultSpeed );
				speedValue = ratio + defaultSpeed;
			}
			
			if (flags.has("a")) {
				fly = walk = true;
			}
			
			if (fly) {
				UtilPlayer.setRawFlySpeed(player, speedValue);
			}
			if (walk) {
				UtilPlayer.setRawWalkSpeed(player, speedValue);
			}
			
			message = "set to " + speed.getData();
		}
		
		String set = null;
		if (fly == walk) {
			set = "Fly and walk";
		} else {
			set = fly ? "Fly" : "Walk";
		}
		
		C.pMain(player, "Speed", set + " speed " + message);
	}
	
}
