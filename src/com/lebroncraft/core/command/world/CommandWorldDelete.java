package com.lebroncraft.core.command.world;

import java.io.IOException;
import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.rank.Rank;

public class CommandWorldDelete
		extends CommandBase {
	
	public CommandWorldDelete() {
		super(Rank.ADMIN, "delete", "d");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() == 0) {
			C.pMain(player, "World", "Please specify a world name");
			return;
		}
		
		String worldName = Util.joinList(args, " ");
		if (UtilWorld.isMainWorld(worldName)) {
			C.pMain(player, "World", "You can't delete the main world");
			return;
		}
		
		if (!UtilWorld.isLoadable(worldName)) {
			C.pMain(player, "World", "World \"" + worldName + "\" does not exist");
			return;
		}
		
		C.pMain(player, "World", "Deleting world...");
		
		try {
			UtilWorld.delete(worldName);
		} catch (IOException ex) {
			C.pErr(ex, player, "Unable to delete world");
			return;
		}
		
		C.pMain(player, "World", "Done");
	}
}
