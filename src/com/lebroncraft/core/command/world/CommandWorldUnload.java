package com.lebroncraft.core.command.world;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.rank.Rank;

public class CommandWorldUnload
extends CommandBase {
	
	public CommandWorldUnload() {
		super(Rank.ADMIN, "unload");
		addFlag("save", Data.NULL);
		addFlag("force", Data.NULL);
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() < 1) {
			C.pMain(player, "World", "Please specify a world name");
			return;
		}
		
		String worldName = Util.joinList(args, " ");
		
		if (!UtilWorld.isLoadable(worldName)) {
			C.pMain(player, "World", "World \"" + worldName + "\" does not exist");
			return;
		}
		
		World world = Bukkit.getWorld(worldName);
		if (world == null) {
			C.pMain(player, "World", "World \"" + worldName + "\" is not loaded");
			return;
		}
		
		boolean save = true;
		if (flags.has("save")) {
			save = false;
		}
		
		boolean unloaded = false;
		String message = null;
		
		if (flags.has("force")) {
			unloaded = UtilWorld.forceUnload(world, save);
			message = unloaded ? "forcibly unloaded" : "failed to forcibly unload";
		} else {
			
			if (UtilWorld.isMainWorld(worldName)) {
				C.pMain(player, "World", "The -force flag is required to unload the main world");
				return;
			}
			
			unloaded = UtilWorld.unload(world, save);
			message = unloaded ? "unloaded" : "failed to unload";
		}
		
		C.pMain(player, "World", "World \"" + worldName + "\" " + message);
	}
	
}
