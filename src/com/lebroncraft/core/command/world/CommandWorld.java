package com.lebroncraft.core.command.world;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;


public class CommandWorld
		extends CommandBase {
		
	public CommandWorld() {
		super(Rank.ADMIN, "world", "wo", "mgw");
		
		addSubCommand(new CommandWorldCreate());
		addSubCommand(new CommandWorldDelete());
		addSubCommand(new CommandWorldChange());
		addSubCommand(new CommandWorldUnload());
		addSubCommand(new CommandWorldList());
		addSubCommand(new CommandWorldPurge());
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		C.pHelp(player, "World", "world change/ch <world>");
		C.pHelp(player, "World", "world unload <world> [-force/-save]");
	}
	
}
