package com.lebroncraft.core.command.world;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.rank.Rank;

public class CommandWorldChange
		extends CommandBase {
	
	public CommandWorldChange() {
		super(Rank.ADMIN, "change", "ch");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() == 0) {
			C.pMain(player, "World", "Please specify a world name");
			return;
		}
		
		String worldName = Util.joinList(args, " ");
		
		if (!UtilWorld.isLoadable(worldName)) {
			C.pMain(player, "World", "World \"" + worldName + "\" does not exist");
			return;
		}
		
		World world = Bukkit.getWorld(worldName);
		
		if (world == null) {
			C.pMain(player, "World", "Loading world \"" + worldName + "\"...");
			world = UtilWorld.load(worldName);
		}
		
		World oldWorld = player.getWorld();
		
		player.teleport(world.getSpawnLocation());
		C.pMain(player, "World", "Teleported you to world \"" + worldName + "\"");
		
		if (!UtilWorld.isMainWorld(oldWorld) && oldWorld.getPlayers().size() == 0) {
			UtilWorld.unload(oldWorld, true);
		}
		
	}
	
}
