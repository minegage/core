package com.lebroncraft.core.command.world;


import java.util.List;

import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilCommand;
import com.lebroncraft.core.common.util.UtilJava;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.rank.Rank;


public class CommandWorldCreate
		extends CommandBase {
		
	public CommandWorldCreate() {
		super(Rank.ADMIN, "create", "cr");
		
		addFlag("seed", Data.LONG);
		addFlag("gen", Data.STRING);
		addFlag("set", Data.STRING);
		addFlag("type", Data.STRING);
		addFlag("env", Data.STRING);
		addFlag("struc", Data.BOOLEAN);
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		if (args.size() == 0) {
			C.pMain(player, "World", "Please specify a world name");
			return;
		}
		
		String worldName = Util.joinList(args, " ");
		if (UtilWorld.isLoadable(worldName)) {
			C.pMain(player, "World", "World \"" + worldName + "\" already exists!");
			return;
		}
		
		WorldCreator creator = WorldCreator.name(worldName);
		
		if (flags.has("gen")) {
			String generatorName = flags.getString();
			creator.generator(generatorName);
			
			if (UtilCommand.failIfTrue(creator.generator() == null, player, "World", "Chunk generator \"" + generatorName
					+ "\" not found")) {
				return;
			}
		}
		
		if (flags.has("type")) {
			String type = flags.getString();
			
			WorldType worldType = UtilJava.parseEnum(WorldType.class, type);
			if (UtilCommand.failIfTrue(worldType == null, player, "World", "World type \"" + type + "\" not found")) {
				return;
			}
			
			creator.type(worldType);
		}
		
		if (flags.has("env")) {
			String env = flags.getString();
			String processed = env.toUpperCase()
					.replaceAll(" ", "_");
					
			try {
				Environment environment = Environment.valueOf(processed);
				creator.environment(environment);
			} catch (IllegalArgumentException ex) {
				C.pMain(player, "World", "Environment \"" + env + "\" not found");
				return;
			}
		}
		
		if (flags.has("struc")) {
			boolean struc = flags.getBoolean();
			creator.generateStructures(struc);
		}
		
		if (flags.has("seed")) {
			Long seed = flags.getLong();
			creator.seed(seed);
		}
		
		if (flags.has("set")) {
			String settings = flags.getString();
			creator.generatorSettings(settings);
		}
		
		C.pMain(player, "World", "Generating world \"" + worldName + "\"...");
		
		try {
			UtilWorld.load(creator);
			C.pMain(player, "World", "Done");
		} catch (Exception ex) {
			C.pErr(ex, player, "Unable to create world");
		}
		
	}
	
}
