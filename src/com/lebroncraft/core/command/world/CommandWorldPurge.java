package com.lebroncraft.core.command.world;


import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.Data;
import com.lebroncraft.core.common.util.UtilWorld;
import com.lebroncraft.core.rank.Rank;


public class CommandWorldPurge
		extends CommandBase {
	
	public CommandWorldPurge() {
		super(Rank.ADMIN, "purge");
		addFlag("all", Data.NULL);
		addFlag("save", Data.NULL);
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		
		boolean all = flags.has("all");
		boolean save = !flags.has("save");
		
		C.pMain(player, "World", "Purging worlds...");
		
		int count = 0;
		
		for (World world : Bukkit.getWorlds()) {
			if (UtilWorld.isMainWorld(world)) {
				continue;
			}
			
			//All flag means automatic unload
			//Otherwise, unload if more than 0 players
			
			if (!all && world.getPlayers().size() > 0) {
				continue;
			}
			
			boolean unloaded = UtilWorld.unload(world, save);
			String message = unloaded ? "unloaded" : "failed to unload";
			C.pMain(player, "World", "World \"" + world + "\"" + message);
			count++;
		}
		
		C.pMain(player, "World", count + " worlds unloaded");
	}
	
}
