package com.lebroncraft.core.command.misc;


import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.rank.Rank;


public class CommandSnap
		extends CommandBase {
		
	public CommandSnap() {
		super(Rank.BUILDER, "snap", "sn");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		String type = "whole";
		
		if (args.size() > 0) {
			type = Util.joinList(args, " ");
		}
		
		Location loc = player.getLocation();
		
		if (type.equals("whole") || type.equals("w")) {
			loc = UtilPos.roundClosestWhole(loc);
		} else if (type.equals("half") || type.equals("h")) {
			loc = UtilPos.roundClosestHalf(loc);
		} else {
			C.pMain(player, "Snap", "Unknown snap type \"" + type + "\"");
			return;
		}
		
		player.teleport(loc);
	}
	
}
