package com.lebroncraft.core.command.misc;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.CorePlugin;
import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.condition.VisibilityManager;
import com.lebroncraft.core.rank.Rank;


public class CommandVanish
		extends CommandBase {
		
	private CorePlugin plugin;
	
	public CommandVanish(CorePlugin plugin) {
		super(Rank.MODERATOR, "vanish", "van");
		
		this.plugin = plugin;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		VisibilityManager vis = plugin.getVisibilityManager();
		
		boolean vanished = !vis.isVanished(player);
		vis.setVanished(player, vanished);
		
		String state = ( vanished ) ? "vanished" : "visible";
		C.pMain(player, "Vanish", "You are now " + C.fElem(state));
	}
	
}
