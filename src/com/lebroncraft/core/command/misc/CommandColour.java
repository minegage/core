package com.lebroncraft.core.command.misc;


import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilString;
import com.lebroncraft.core.rank.Rank;


public class CommandColour
		extends CommandBase {
		
	private static final char SQUARE = '\u2B1B';
	private static final String SQUARES;
	
	static {
		String squaresTemp = "";
		for (int i = 0; i < 15; i++) {
			squaresTemp += SQUARE;
		}
		
		SQUARES = squaresTemp;
	}
	
	public CommandColour() {
		super(Rank.BUILDER, "colour", "colours", "color", "colors");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		
		String type = "colour";
		
		if (args.size() > 0) {
			type = args.get(0)
					.toLowerCase();
		}
		
		if (type.startsWith("colour") || type.startsWith("color")) {
			for (String str : C.bow()) {
				
				// Convert to chatcolour so it can be printed
				for (ChatColor colour : ChatColor.values()) {
					if (colour.toString()
							.equals(str)) {
						print(player, colour);
					}
				}
			}
		} else if (type.equals("all")) {
			for (ChatColor colour : ChatColor.values()) {
				print(player, colour);
			}
		} else if (type.equals("format")) {
			for (ChatColor colour : ChatColor.values()) {
				if (colour.isFormat()) {
					print(player, colour);
				}
			}
		} else {
			C.pMain(player, "Colour", "Unknown colour filter " + C.fElem(type) + ". Valid filters are: " + C.fElem(
					"colour, all, format"));
		}
	}
	
	private void print(Player player, ChatColor colour) {
		String name = UtilString.format(colour.name());
		
		char character = colour.getChar();
		C.pGeneral(player, " " + Character.toUpperCase(character), colour + SQUARES + C.cReset + " (" + name + ")");
	}
	
}
