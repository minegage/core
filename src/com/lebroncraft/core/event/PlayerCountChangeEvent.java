package com.lebroncraft.core.event;


import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.lebroncraft.core.common.util.UtilServer;


public class PlayerCountChangeEvent
		extends PlayerEvent {
		
	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	private int countEffect;
	
	public PlayerCountChangeEvent(Player player, int countEffect) {
		super(player);
		this.countEffect = countEffect;
	}
	
	public int getNewPlayerCount() {
		return UtilServer.numPlayers() + countEffect;
	}
	
}
