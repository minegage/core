package com.lebroncraft.core.event;


import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import com.google.common.collect.Lists;
import com.lebroncraft.core.combat.CombatDamage;
import com.lebroncraft.core.combat.DamageHistory;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.token.EntityToken;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.spawn.SpawnManager;


/**
 * Player death event wrapper. Facilitates getting the killed Player and direct/indirect killer
 */
public class CustomDeathEvent
		extends Event {
		
	private static final HandlerList handlers = new HandlerList();
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	private List<ItemStack> drops = Lists.newArrayList();
	private Location respawnLocation = null;
	
	private LivingEntity killed;
	private boolean killedPlayer;
	
	/* The "real" damager. Is the shooter of a projectile, or else it is the same as the direct
	 * killer. This should always be used to get the damager */
	private ProjectileSource indirectKiller;
	
	/* The entity which dealt the damage to the player. Could be another entity, a projectile, etc. */
	private Entity directKiller;
	
	private boolean killerPlayer;
	
	private EntityDamageEvent event;
	private DamageHistory damageHistory;
	private LivingEntity credittedKiller;
	
	private SafeMap<OfflinePlayer, Double> assists = new SafeMap<>();
	
	public CustomDeathEvent(EntityDamageEvent event, DamageHistory damageHistory) {
		killed = (LivingEntity) event.getEntity();
		killedPlayer = ( killed instanceof Player );
		respawnLocation = SpawnManager.instance.getSpawnpoint(killed.getWorld());
		
		directKiller = UtilEvent.getDirectDamager(event);
		indirectKiller = UtilEvent.getIndirectDamager(event);
		killerPlayer = ( indirectKiller != null ) && ( indirectKiller instanceof Player );
		
		this.event = event;
		this.damageHistory = damageHistory;
		
		double totalDamage = damageHistory.getDamage();
		
		CombatDamage creditKiller = damageHistory.getMostRecentLivingDamager();
		if (creditKiller != null) {
			this.credittedKiller = (LivingEntity) creditKiller.indirectDamager.getEntity()
					.getEntity();
		}
		
		for (Entry<EntityToken, Double> entry : damageHistory.getTotalDamage()
				.entrySet()) {
				
			// Don't count assists from non-players
			EntityToken token = entry.getKey();
			if (!token.isPlayer()) {
				continue;
			}
			
			// Don't give the killer assists
			OfflinePlayer player = token.getPlayer();
			if (credittedKiller != null && credittedKiller.getUniqueId()
					.equals(player.getUniqueId())) {
				continue;
			}
			
			Double damage = entry.getValue();
			
			// Only count the assist if the player has done at least 10% of the damage
			double percentage = damage / totalDamage;
			if (percentage > 0.1) {
				assists.put(player, damage);
			}
			
		}
	}
	
	public SafeMap<OfflinePlayer, Double> getAssists() {
		return assists;
	}
	
	public LivingEntity getCredittedKiller() {
		return credittedKiller;
	}
	
	public boolean isPlayerCreditted() {
		return credittedKiller != null && credittedKiller.getType() == EntityType.PLAYER;
	}
	
	public Player getPlayerCreditted() {
		return (Player) credittedKiller;
	}
	
	public EntityDamageEvent getCause() {
		return event;
	}
	
	public List<ItemStack> getDrops() {
		return drops;
	}
	
	public void setDrops(List<ItemStack> drops) {
		this.drops = drops;
	}
	
	public Location getRespawnLocation() {
		return respawnLocation;
	}
	
	public void setRespawnLocation(Location respawnLocation) {
		this.respawnLocation = respawnLocation;
	}
	
	public LivingEntity getKilled() {
		return killed;
	}
	
	public boolean isPlayerKilled() {
		return killedPlayer;
	}
	
	public Player getKilledPlayer() {
		return (Player) killed;
	}
	
	public ProjectileSource getIndirectKiller() {
		return indirectKiller;
	}
	
	public Entity getDirectKiller() {
		return directKiller;
	}
	
	public boolean isPlayerKiller() {
		return killerPlayer;
	}
	
	public Player getKillerPlayer() {
		if (!killerPlayer) {
			return null;
		}
		return (Player) indirectKiller;
	}
	
	public DamageHistory getDamageHistory() {
		return damageHistory;
	}
	
	
	
}
