package com.lebroncraft.core.event;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.common.Click.ClickButton;
import com.lebroncraft.core.common.Click.ClickTarget;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.spawn.SpawnManager;


/**
 * Chains existing events into custom events (eg death -> custom death)
 */
public class EventManager
		extends PluginModule {
		
	private SpawnManager spawnManager;
	
	public EventManager(JavaPlugin plugin) {
		super("Event Manager", plugin);
		
		spawnManager = new SpawnManager(plugin);
	}
	
	private void callCountChange(Player player, int countEffect) {
		PlayerCountChangeEvent listEvent = new PlayerCountChangeEvent(player, countEffect);
		UtilEvent.call(listEvent);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void listUpdate(PlayerJoinEvent event) {
		callCountChange(event.getPlayer(), 0);
	}
	
	@EventHandler
	public void listUpdate(PlayerQuitEvent event) {
		callCountChange(event.getPlayer(), -1);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void filterLogin(PlayerLoginEvent event) {
		RankManager rankManager = RankManager.instance;
		Server server = Bukkit.getServer();
		Player player = event.getPlayer();
		
		if (server.hasWhitelist() && !rankManager.hasPermission(player, Rank.MODERATOR) && !player.isWhitelisted()) {
			event.disallow(Result.KICK_WHITELIST, ChatColor.YELLOW + "You must be on the whitelist to join!");
		}
	}
	
	/* Entity right/left clicks */
	private void callEntityClick(Entity clicked, Player clicker, ClickButton click) {
		ClickEntityEvent event = new ClickEntityEvent(clicked, clicker, new Click(click, ClickTarget.AIR, clicker.isSneaking()));
		UtilEvent.call(event);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onLeftClickEntity(EntityDamageByEntityEvent event) {
		Entity clicker = event.getDamager();
		
		if (!( clicker instanceof Player )) {
			return;
		}
		
		Entity clicked = event.getEntity();
		Player pClicker = (Player) clicker;
		
		callEntityClick(clicked, pClicker, ClickButton.LEFT);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onRightClickEntity(PlayerInteractEntityEvent event) {
		Player clicker = event.getPlayer();
		Entity clicked = event.getRightClicked();
		
		callEntityClick(clicked, clicker, ClickButton.RIGHT);
	}
	
	/**
	 * In some cases, PlayerInteractEvent will be cancelled by default. This uncancels the event at
	 * the lowest priority level to give the event predictable behaviour at higher priorities.
	 * 
	 * @param event
	 */
	@EventHandler(priority = EventPriority.LOWEST)
	public void correctPlayerInteract(PlayerInteractEvent event) {
		event.setCancelled(false);
	}
	
	/**
	 * If the player has clicked and item and the event was cancelled, update their inventory,
	 * because the item may have been changed but not updated.
	 * 
	 * @param event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void updateInventory(PlayerInteractEvent event) {
		if (event.isCancelled() && event.getItem() != null && event.getItem()
				.getType() != Material.AIR) {
			event.getPlayer()
					.updateInventory();
		}
	}
	
	public SpawnManager getSpawnManager() {
		return spawnManager;
	}
	
}
