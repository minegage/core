package com.lebroncraft.core.npc;


import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.mob.MobManager;
import com.lebroncraft.core.npc.command.CommandNPC;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;


public class NPCManager
		extends MobManager<NPC> {
		
	public NPCManager(JavaPlugin plugin) {
		super("NPC Manager", plugin, "npcs.txt");
		
		registerCommand(new CommandNPC(this));
	}
	
	@Override
	public String serializeMob(NPC npc) {
		LivingEntity entity = npc.getEntity();
		
		String uid = entity.getUniqueId()
				.toString();
		String loc = UtilPos.serializeLocation(entity.getLocation());
		String radius = npc.getRange() + "";
		
		String serialized = uid + ":" + loc + ":" + radius;
		return serialized;
	}
	
	@Override
	public NPC deserializeMob(World world, String serialized) {
		String[] split = serialized.split(":");
		String serializedUID = split[0];
		String serializedLoc = split[1];
		String serializedRadius = split[2];
		
		Location location = UtilPos.deserializeLocation(serializedLoc, world);
		UUID uid = UUID.fromString(serializedUID);
		double radius = Double.parseDouble(serializedRadius);
		
		NPC npc = new NPC(uid, location, radius);
		return npc;
	}
	
	@EventHandler
	public void returnToPost(TickEvent event) {
		if (!event.getTick()
				.equals(Tick.TICK_5)) {
			return;
		}
		
		for (NPC npc : mobs) {
			if (npc.getEntity() == null) {
				continue;
			}
			
			if (!npc.isInPost()) {
				npc.returnToPost();
				
				if (npc.failedPostChecks++ > 15) {
					npc.teleportToPost();
					npc.failedPostChecks = 0;
				}
				
			} else {
				npc.failedPostChecks = 0;
				if (npc.isReturning()) {
					UtilEntity.clearTarget(npc.getEntity());
					npc.setReturning(false);
				}
			}
		}
	}
	
}
