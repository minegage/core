package com.lebroncraft.core.npc;


import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.mob.Mob;

import net.minecraft.server.v1_8_R2.EntityCreature;
import net.minecraft.server.v1_8_R2.EntityHuman;
import net.minecraft.server.v1_8_R2.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_8_R2.PathfinderGoalRandomLookaround;


public class NPC
		extends Mob {
		
	protected boolean returning = false;
	protected double radius;
	protected int failedPostChecks = 0;
	protected double moveSpeed = 0.0;
	protected boolean silent = true;
	
	public NPC(UUID uid, Location location, double radius) {
		super(uid, location);
		this.radius = radius;
	}
	
	@Override
	public void load(LivingEntity entity) {
		super.load(entity);
		
		this.moveSpeed = UtilEntity.getSpeed(entity);
		
		EntityCreature creature = UtilEntity.getNMSEntityCreature(entity);
		
		UtilEntity.clearPathfinderGoals(entity);
		creature.setCustomNameVisible(true);
		
		creature.goalSelector.a(0, new PathfinderGoalRandomStrollCustom(creature, moveSpeed, getPost(), radius));
		creature.goalSelector.a(1, new PathfinderGoalLookAtPlayer(creature, EntityHuman.class, 3.0F));
		creature.goalSelector.a(2, new PathfinderGoalRandomLookaround(creature));
		
		UtilEntity.setSilent(entity, silent);
		
	}
	
	public boolean isInPost() {
		Vector currentPos = entity.getLocation()
				.toVector();
		return post.distanceSquared(currentPos) <= Math.pow(radius, 2D);
	}
	
	public boolean isReturning() {
		return returning;
	}
	
	public void setReturning(boolean returning) {
		this.returning = returning;
	}
	
	public void returnToPost() {
		UtilEntity.setTarget(entity, post.getX(), post.getY(), post.getZ(), moveSpeed);
		setReturning(true);
	}
	
	public double getRange() {
		return radius;
	}
	
	public int getFailedPostChecks() {
		return failedPostChecks;
	}
	
	public void setFailedPostChecks(int failedPostChecks) {
		this.failedPostChecks = failedPostChecks;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getMoveSpeed() {
		return moveSpeed;
	}
	
	public void setMoveSpeed(double moveSpeed) {
		this.moveSpeed = moveSpeed;
	}
	
	public boolean isSilent() {
		return silent;
	}
	
	public void setSilent(boolean silent) {
		this.silent = silent;
	}
	
}
