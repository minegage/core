package com.lebroncraft.core.npc.command;


import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.data.DataDouble;
import com.lebroncraft.core.common.util.Util;
import com.lebroncraft.core.common.util.UtilCommand;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.mob.command.manager.CommandMobManager;
import com.lebroncraft.core.mob.command.manager.CreateToken;
import com.lebroncraft.core.npc.NPC;
import com.lebroncraft.core.npc.NPCManager;


public class CommandNPC
		extends CommandMobManager<NPCManager, NPC> {
		
	public CommandNPC(NPCManager manager) {
		super(manager, "NPC");
	}
	
	@Override
	public CreateToken newToken() {
		return new NPCToken();
	}
	
	public class NPCToken
			extends CreateToken {
			
		private String name;
		private Location location;
		private double radius;
		
		@Override
		public boolean create(Player player, List<String> args, Flags flags) {
			if (args.size() < 1) {
				C.pMain(player, "NPC", "Please specify a radius");
				return false;
			}
			
			DataDouble radius = new DataDouble();
			if (UtilCommand.failedParse(radius, args.get(0), player, "NPC", "Radius \"" + args.get(0) + "\" is invalid")) {
				return false;
			}
			
			String name = "";
			if (args.size() > 1) {
				name = Util.joinList(args, " ", 1);
				name = C.translate(name);
			}
			
			Location location = player.getLocation();
			
			this.name = name;
			this.radius = radius.getData();
			this.location = location;
			
			return true;
		}
		
		@Override
		public String create(LivingEntity entity) {
			entity.setCustomName(name);
			
			String uid = entity.getUniqueId()
					.toString();
			String loc = UtilPos.serializeLocation(location);
			String rad = radius + "";
			
			return uid + ":" + loc + ":" + rad;
		}
	}
	
}
