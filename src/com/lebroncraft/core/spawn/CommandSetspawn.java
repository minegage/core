package com.lebroncraft.core.spawn;


import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.rank.Rank;


public class CommandSetspawn
		extends CommandModule<SpawnManager> {
		
	public CommandSetspawn(SpawnManager spawnManager) {
		super(spawnManager, Rank.ADMIN, "setspawn");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		
		Location location = player.getLocation();
		World world = location.getWorld();
		
		File file = new File(world.getWorldFolder(), "spawn.txt");
		
		if (args.contains("remove")) {
			if (!file.exists()) {
				C.pMain(player, "Spawn", "The spawnpoint isn't set");
			} else {
				boolean deleted = file.delete();
				
				if (deleted) {
					getPlugin().removeSpawnpoint(world);
					C.pMain(player, "Spawn", "Spawnpoint removed");
				} else {
					C.pMain(player, "Spawn", "Failed to remove spawnpoint");
				}
			}
			
		} else {
			try {
				file.createNewFile();
				
				String serialized = UtilPos.serializeLocation(location);
				FileUtils.write(file, serialized);
				
				getPlugin().setSpawnpoint(world, location);
				
				C.pMain(player, "Spawn", "Spawnpoint set");
				
			} catch (IOException ex) {
				C.pErr(ex, player, "Unable to set spawnpoint");
			}
		}
		
	}
	
}
