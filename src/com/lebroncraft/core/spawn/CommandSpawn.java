package com.lebroncraft.core.spawn;


import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.rank.Rank;


public class CommandSpawn
		extends CommandModule<SpawnManager> {
		
	public CommandSpawn(SpawnManager spawnManager) {
		super(spawnManager, Rank.MEMBER, "spawn");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		World world = player.getWorld();
		Location spawnpoint = getPlugin().getSpawnpoint(world);
		
		SpawnCommandEvent event = new SpawnCommandEvent(player);
		UtilEvent.call(event);
		
		if (!event.isCancelled()) {
			player.teleport(spawnpoint);
		}
	}
	
}
