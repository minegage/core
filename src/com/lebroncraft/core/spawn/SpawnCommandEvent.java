package com.lebroncraft.core.spawn;


import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import com.lebroncraft.core.event.EventCancellable;


public class SpawnCommandEvent
		extends EventCancellable {
		
	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
	private Player player;
	
	public SpawnCommandEvent(Player player) {
		this.player = player;
	}
	
	public Player getPlayer() {
		return player;
	}
	
}
