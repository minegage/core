package com.lebroncraft.core.spawn;


import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.player.PlayerSpawnLocationEvent;

import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;


public class SpawnManager
		extends PluginModule {
		
	public static final String FILE_NAME = "spawn.txt";
	
	private SafeMap<World, Location> spawnpoints = new SafeMap<>();
	public boolean overrideSpawns = true;
	
	public static SpawnManager instance;
	
	public SpawnManager(JavaPlugin plugin) {
		super("Spawn Manager", plugin);
		
		for (World world : Bukkit.getWorlds()) {
			loadSpawnpoint(world);
		}
		
		registerCommand(new CommandSetspawn(this));
		registerCommand(new CommandSpawn(this));
		
		SpawnManager.instance = this;
	}
	
	@Override
	protected void onDisable() {
		spawnpoints.clear();
	}
	
	public Location getSpawnpoint(World world) {
		return spawnpoints.getOrDefault(world, world.getSpawnLocation());
	}
	
	public void setSpawnpoint(World world, Location location) {
		spawnpoints.put(world, location);
	}
	
	private void loadSpawnpoint(World world) {
		File file = new File(world.getWorldFolder(), FILE_NAME);
		if (!file.exists()) {
			return;
		}
		
		try {
			String content = FileUtils.readFileToString(file);
			Location spawnpoint = UtilPos.deserializeLocation(content, world);
			spawnpoints.put(world, spawnpoint);
		} catch (IOException ex) {
			L.error(ex, "Unable to load spawnpoint for world \"" + world.getName() + "\"");
		}
	}
	
	@EventHandler
	public void loadSpawnpoint(WorldLoadEvent event) {
		loadSpawnpoint(event.getWorld());
	}
	
	public void removeSpawnpoint(World world) {
		spawnpoints.remove(world);
	}
	
	@EventHandler
	public void unloadSpawnpoint(WorldUnloadEvent event) {
		removeSpawnpoint(event.getWorld());
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onTeleport(PlayerTeleportEvent event) {
		Location newSpawn = getNewSpawn(event.getTo());
		
		if (newSpawn != null) {
			event.setTo(newSpawn);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRespawn(PlayerRespawnEvent event) {
		if (!overrideSpawns) {
			return;
		}
		
		Location newSpawn = getNewSpawn(event.getRespawnLocation());
		
		if (newSpawn != null) {
			event.setRespawnLocation(newSpawn);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onSpawn(PlayerSpawnLocationEvent event) {
		if (!overrideSpawns) {
			return;
		}
		
		Location newSpawn = getNewSpawn(event.getSpawnLocation());
		
		if (newSpawn != null) {
			event.setSpawnLocation(newSpawn);
		}
	}
	
	// Set the join location to the world spawn by default. Low priority to allow overriding
	@EventHandler(priority = EventPriority.LOWEST)
	public void setJoinLocation(PlayerSpawnLocationEvent event) {
		if (!overrideSpawns) {
			return;
		}
		
		event.setSpawnLocation(event.getSpawnLocation()
				.getWorld()
				.getSpawnLocation());
	}
	
	/**
	 * @param targetSpawn
	 *        The target spawnpoint for the event (PlayerSpawnLocationEvent or PlayerRespawnEvent)
	 * @return If targetSpawn matches the spawn location of its world, it will return the custom
	 *         spawnpoint (if it exists). Otherwise returns null.
	 */
	private Location getNewSpawn(Location targetSpawn) {
		if (targetSpawn == null) {
			return null;
		}
		
		World targetWorld = targetSpawn.getWorld();
		Location worldSpawn = targetWorld.getSpawnLocation();
		
		if (targetSpawn.equals(worldSpawn)) {
			Location realSpawn = spawnpoints.get(targetWorld);
			return realSpawn;
		}
		
		return null;
	}
	
}
