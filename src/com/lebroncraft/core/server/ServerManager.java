package com.lebroncraft.core.server;


import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.Messenger;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;


public class ServerManager
		extends PluginModule
		implements PluginMessageListener {
	
	public static final String LOBBY = "MGLobby";
	public static ServerManager instance;
	
	private Set<Player> connecting = new HashSet<>();
	
	public ServerManager(JavaPlugin plugin) {
		super("Server Manager", plugin);
		ServerManager.instance = this;
	}
	
	@Override
	protected void onEnable() {
		Messenger messenger = plugin.getServer()
				.getMessenger();
		messenger.registerOutgoingPluginChannel(plugin, "BungeeCord");
		messenger.registerIncomingPluginChannel(plugin, "BungeeCord", this);
	}
	
	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] message) {
		if (!channel.equals("BungeeCord")) {
			return;
		}
		
		EventPluginMessageReceive event = new EventPluginMessageReceive(message);
		UtilEvent.call(event);
	}
	
	/**
	 * @param server
	 *        The name of the Bungeecord server
	 * @param force
	 *        If true, ignores existing connection attempt
	 * @param notify
	 *        If true, notifies the player of the connection attempt
	 */
	public void connect(Player player, String server) {
		if (connecting.contains(player)) {
			return;
		}
		
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		
		try {
			out.writeUTF("Connect");
			out.writeUTF(server);
			
			player.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
		} catch (IOException ex) {
			L.error(ex, "Unable to connect " + player.getName() + " to " + server);
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void clearReference(PlayerQuitEvent event) {
		connecting.remove(event.getPlayer());
	}
}
