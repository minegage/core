package com.lebroncraft.core.board;


import java.util.Set;

import org.apache.commons.lang3.Validate;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.lebroncraft.core.common.C;


public class ObjectiveSide
		extends ObjectiveWrapper {
		
	/* A sidebar objective can have up to 15 rows. Each row must have a score. Rows are
	 * automatically (and unavoidably) displayed in decreasing order of their score, from the top of
	 * the scoreboard to the bottom. Here, the score of a row is referred to as its row number (or
	 * "row num") */
	
	public ObjectiveSide(Scoreboard board, String criteria) {
		super(board, criteria, DisplaySlot.SIDEBAR);
	}
	
	public ObjectiveSide(Scoreboard board) {
		super(board, DisplaySlot.SIDEBAR);
	}
	
	public ObjectiveSide(Objective other) {
		super(other);
	}
	
	public void setHeader(String header) {
		setDisplayName(header);
	}
	
	public void setRow(int rowNum, String entry) {
		Validate.notNull(entry, "Entry cannot be null");
		
		String padded = padEntry(entry);
		setScore(padded, rowNum);
	}
	
	public void updateRow(int rowNum, String content) {
		Validate.notNull(content, "Content cannot be null");
		
		Score score = getScore(rowNum);
		
		// Don't update if the content hasn't changed
		if (score.getEntry()
				.equals(content)) {
			return;
		}
		
		removeRow(score);
		setScore(padEntry(content), rowNum);
	}
	
	public int addRow(String content) {
		Validate.notNull(content, "Content cannot be null");
		
		int rowNum = nextRowNum();
		setRow(rowNum, content);
		return rowNum;
	}
	
	public boolean hasRoom() {
		return getPositiveScores().size() < 15;
	}
	
	public String getSpacer() {
		return C.cReset;
	}
	
	public void removeRow(Score score) {
		deleteScore(score);
	}
	
	public void removeRow(int rowNum) {
		Score score = getScore(rowNum);
		if (score != null) {
			removeRow(score);
		}
	}
	
	public int nextRowNum() {
		return 15 - getPositiveScores().size();
	}
	
	public String padEntry(String entry) {
		Set<String> entries = getPositiveEntries();
		
		entry = padEntry(entry, entries);
		
		if (!isSafeEntry(entry)) {
			throw new IllegalStateException("Padding sidebar objective entry failed");
		}
		
		return entry;
	}
	
	private String padEntry(String entry, Set<String> entries) {
		for (String otherEntry : entries) {
			if (entry.equals(otherEntry)) {
				entry += getSpacer();
				entry = padEntry(entry, entries);
				break;
			}
		}
		
		return entry;
	}
	
}
