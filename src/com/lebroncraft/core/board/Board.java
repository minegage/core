package com.lebroncraft.core.board;


import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;


public class Board {
	
	private int objCount = 1;
	private int teamCount = 1;
	
	protected Scoreboard board;
	protected ObjectiveSide sideObjective;
	protected ObjectiveTag tagObjective;
	protected ObjectiveTab tabObjective;
	
	public Board() {
		board = Bukkit.getScoreboardManager()
				.getNewScoreboard();
	}
	
	public Board(Board other) {
		this();
		
		Scoreboard otherBoard = other.getBoard();
		
		for (Team otherTeam : otherBoard.getTeams()) {
			Team team = board.registerNewTeam(otherTeam.getName());
			team.setDisplayName(otherTeam.getDisplayName());
			team.setPrefix(otherTeam.getPrefix());
			team.setSuffix(otherTeam.getSuffix());
			
			team.setAllowFriendlyFire(otherTeam.allowFriendlyFire());
			team.setCanSeeFriendlyInvisibles(otherTeam.canSeeFriendlyInvisibles());
			team.setNameTagVisibility(otherTeam.getNameTagVisibility());
		}
		
		ObjectiveSide otherSide = other.getSideObjective();
		ObjectiveTag otherTag = other.getTagObjective();
		ObjectiveTab otherTab = other.getTabObjective();
		
		for (Objective obj : otherBoard.getObjectives()) {
			Objective copy = board.registerNewObjective(obj.getName(), obj.getCriteria());
			copy.setDisplayName(obj.getDisplayName());
			copy.setDisplaySlot(obj.getDisplaySlot());
			
			for (String entry : otherBoard.getEntries()) {
				Score score = obj.getScore(entry);
				int value = score.getScore();
				
				if (value != 0) {
					copy.getScore(entry)
							.setScore(value);
				}
			}
			
			if (otherSide.getObjective()
					.equals(copy)) {
				sideObjective = new ObjectiveSide(copy);
			} else if (otherTag.getObjective()
					.equals(copy)) {
				tagObjective = new ObjectiveTag(copy);
			} else if (otherTab.getObjective()
					.equals(copy)) {
				tabObjective = new ObjectiveTab(copy);
			}
		}
	}
	
	public Team createTeam() {
		return board.registerNewTeam("t" + teamCount++);
	}
	
	public Scoreboard getBoard() {
		return board;
	}
	
	public ObjectiveSide setSideObjective() {
		sideObjective = new ObjectiveSide(board);
		return sideObjective;
	}
	
	public ObjectiveTag setTagObjective() {
		tagObjective = new ObjectiveTag(board);
		return tagObjective;
	}
	
	public ObjectiveTab setTabObjective() {
		tabObjective = new ObjectiveTab(board);
		return tabObjective;
	}
	
	public void setSideObjective(ObjectiveSide sideObjective) {
		this.sideObjective = sideObjective;
	}
	
	public void setTagObjective(ObjectiveTag tagObjective) {
		this.tagObjective = tagObjective;
	}
	
	public void setTabObjective(ObjectiveTab tabObjective) {
		this.tabObjective = tabObjective;
	}
	
	public ObjectiveSide getSideObjective() {
		return sideObjective;
	}
	
	public ObjectiveTag getTagObjective() {
		return tagObjective;
	}
	
	public ObjectiveTab getTabObjective() {
		return tabObjective;
	}
	
	int nextTeamCount() {
		return teamCount++;
	}
	
	int nextObjCount() {
		return objCount++;
	}
	
}
