package com.lebroncraft.core.board;


import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilEvent;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.module.PluginModule;


public class BoardManager
		extends PluginModule {
		
	private SafeMap<Player, Board> boards = new SafeMap<Player, Board>();
	
	public BoardManager(JavaPlugin plugin) {
		super("Board Manager", plugin);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void clearReferences(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		removeBoard(player);
	}
	
	public void setBoard(Player player, Board board) {
		player.setScoreboard(board.getBoard());
		boards.put(player, board);
		
		AssignBoardEvent event = new AssignBoardEvent(player, board);
		UtilEvent.call(event);
	}
	
	public Board removeBoard(Player player) {
		player.setScoreboard(UtilUI.getNewScoreboard());
		return boards.remove(player);
	}
	
	public Board getBoard(Player player) {
		return boards.get(player);
	}
	
	public Set<Board> getPlayerBoards() {
		return new HashSet<>(boards.values());
	}
	
	public SafeMap<Player, Board> getBoards() {
		return boards;
	}
	
}
