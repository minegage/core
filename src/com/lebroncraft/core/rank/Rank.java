package com.lebroncraft.core.rank;


import com.lebroncraft.core.common.C;


public enum Rank {
	
	MEMBER("member", "Member", C.rMember + C.cBold + "Member", C.rMember),
	PRO("pro", "Pro", C.rPro + C.cBold + "Pro", C.rPro),
	ACE("bruh", "Ace", C.rAce + C.cBold + "Ace", C.rAce),
	MVP("m8", "MVP", C.rMvp + C.cBold + "MVP", C.rMvp),
	YOUTUBE("youtube", "YouTube", C.cBold + "You" + C.rYoutube2 + C.cBold + "Tube", C.rYoutubeName),
	BUILDER("builder", "Builder", C.rBuilder + C.cBold + "Builder", C.rBuilder),
	MODERATOR("moderator", "Moderator", C.rMod + C.cBold + "Mod", C.rMod),
	ADMIN("admin", "Admin", C.rAdmin + C.cBold + "Admin", C.rAdmin),
	DEVELOPER("developer", "Developer", C.rDev + C.cBold + "Dev", C.rDev),
	OWNER("owner", "Owner", C.rOwner + C.cBold + "Owner", C.rOwner);
	
	private String chatColor;
	
	private String permName;
	private String fullName;
	private String displayName;
	
	
	private Rank(String permName, String fullName, String displayName, String chatColor) {
		this.permName = permName;
		this.fullName = fullName;
		this.displayName = displayName;
		this.chatColor = chatColor;
	}
	
	public String getColor() {
		return chatColor;
	}
	
	public String getPermName() {
		return permName;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public String getTeamPrefix() {
		return getDisplayName() + C.cReset + " ";
	}
	
	public String getChatPrefix() {
		return getTeamPrefix() + chatColor;
	}
	
	public String getTeamName() {
		return name();
	}
	
	public boolean includes(Rank rank) {
		if (rank == null) {
			return true;
		}
		
		return compareTo(rank) >= 0;
	}
}
