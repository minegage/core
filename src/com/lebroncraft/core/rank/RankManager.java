package com.lebroncraft.core.rank;


import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.tyrannyofheaven.bukkit.zPermissions.ZPermissionsService;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.log.L;
import com.lebroncraft.core.module.PluginModule;


public class RankManager
		extends PluginModule {
		
	public static RankManager instance;
	
	private ZPermissionsService zperms;
	
	public SafeMap<Player, Rank> ranks = new SafeMap<>();
	
	public RankManager(JavaPlugin plugin) {
		super("Rank Manager", plugin);
		RankManager.instance = this;
		
		this.zperms = getServer().getServicesManager()
				.load(ZPermissionsService.class);
				
		if (this.zperms == null) {
			L.severe("Unable to load ZPermissions service");
		}
		
		for (Player player : UtilServer.players()) {
			loadRank(player);
		}
	}
	
	public Rank getRank(Player player) {
		return ranks.get(player);
	}
	
	public boolean hasPermission(Rank rank, Rank minRank) {
		return rank.includes(minRank);
	}
	
	public boolean hasPermission(Player player, Rank minRank) {
		return player.isOp() || hasPermission(getRank(player), minRank);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void clearReference(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		ranks.remove(player);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void loadRank(PlayerLoginEvent event) {
		loadRank(event.getPlayer());
	}
	
	public void loadRank(Player player) {
		String primaryGroup = "null";
		if (zperms != null) {
			UUID uid = player.getUniqueId();
			primaryGroup = zperms.getPlayerAssignedGroups(uid)
					.get(0);
		}
		
		Rank rank = null;
		
		// Find rank with the name of the player group
		for (Rank other : Rank.values()) {
			if (other.getPermName()
					.equalsIgnoreCase(primaryGroup)) {
				rank = other;
				break;
			}
		}
		
		// Failsafe in case zperms doesn't load
		if (zperms == null && player.isOp()) {
			
			rank = Rank.ADMIN;
			UUID uid = player.getUniqueId();
			
			runSyncDelayed(20L, new Runnable() {
				@Override
				public void run() {
					Player p = Bukkit.getPlayer(uid);
					if (p != null) {
						C.pWarn(p, "Warning", "ZPermissionsService failed to load. Ranks will not work.");
					}
				}
			});
		}
		
		// Default to member if rank isn't found
		if (rank == null) {
			rank = Rank.MEMBER;
			L.warn("Unknown rank " + primaryGroup + "; defaulting " + player.getName() + " to Member group");
		}
		
		String prefix = rank.getChatPrefix();
		String displayName = prefix + player.getName() + ChatColor.RESET;
		
		player.setDisplayName(displayName);
		ranks.put(player, rank);
	}
	
}
