package com.lebroncraft.core.move;


import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;


public class MoveManager
		extends PluginModule {
		
	private SafeMap<UUID, PlayerMove> playerMovement = new SafeMap<>();
	
	public MoveManager(JavaPlugin plugin) {
		super("Move Manager", plugin);
	}
	
	@EventHandler
	public void onUpdate(TickEvent event) {
		if (!event.getTick()
				.equals(Tick.TICK_1)) {
			return;
		}
		
		for (Entry<UUID, PlayerMove> entry : playerMovement.entrySet()) {
			OfflinePlayer player = plugin.getServer()
					.getOfflinePlayer(entry.getKey());
					
			PlayerMove movement = entry.getValue();
			
			Location lastLocation = movement.lastLocation;
			Location currentLocation = ( (Player) player ).getLocation();
			
			Vector lastPosition = lastLocation.toVector();
			Vector currentPosition = currentLocation.toVector();
			
			if (lastPosition.subtract(currentPosition)
					.lengthSquared() != 0) {
				movement.lastMoved = System.currentTimeMillis();
				movement.lastLocation = currentLocation;
			}
		}
		
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		PlayerMove movement = new PlayerMove(player.getLocation());
		playerMovement.put(player.getUniqueId(), movement);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		playerMovement.remove(event.getPlayer()
				.getUniqueId());
	}
	
	public PlayerMove getMove(Player player) {
		return getMove(player.getUniqueId());
	}
	
	public PlayerMove getMove(UUID uid) {
		return playerMovement.get(uid);
	}
	
}
