package com.lebroncraft.core.menu;


import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;

import com.lebroncraft.core.common.util.Util;


public abstract class MenuTemp
		extends Menu {
		
	protected Player player;
	
	public MenuTemp(MenuManager manager, String title, int rows, String prevRawName, Player player) {
		super(manager, title, "tempinv" + Util.nextID(), rows, prevRawName);
		this.player = player;
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent event) {
		if (!event.getInventory()
				.equals(recentInventory)) {
			return;
		}
		
		if (!event.getPlayer()
				.equals(player)) {
			return;
		}
		
		dispose();
	}
	
}
