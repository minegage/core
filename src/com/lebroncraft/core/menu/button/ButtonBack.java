package com.lebroncraft.core.menu.button;


import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.menu.Menu;
import com.lebroncraft.core.menu.MenuManager;


public class ButtonBack
		extends Button {
		
	private String prevRawName;
	private MenuManager manager;
	
	public ButtonBack(Menu menu) {
		this.prevRawName = menu.getPrevRawName();
		this.manager = menu.getMenuManager();
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		if (prevRawName == "close") {
			player.closeInventory();
		} else {
			manager.open(player, prevRawName);
		}
		
		return true;
	}
	
}
