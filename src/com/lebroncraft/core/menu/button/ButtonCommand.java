package com.lebroncraft.core.menu.button;


import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;


public class ButtonCommand
		extends Button {
		
	private String command;
	
	public ButtonCommand(String command) {
		this.command = command;
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		player.chat("/" + command);
		return true;
	}
	
}
