package com.lebroncraft.core.menu.button;


import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.server.ServerManager;


public class ButtonServer
		extends Button {
		
	private String serverName;
	private ServerManager serverManager;
	
	public ButtonServer(ServerManager serverManager, String serverName) {
		this.serverManager = serverManager;
		this.serverName = serverName;
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		serverManager.connect(player, serverName);
		return true;
	}
	
}
