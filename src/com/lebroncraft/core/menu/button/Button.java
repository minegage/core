package com.lebroncraft.core.menu.button;


import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.common.SoundToken;


public abstract class Button {
	
	public static final SoundToken SUCCESSFUL = new SoundToken(Sound.WOOD_CLICK, 1F, 1F);
	public static final SoundToken UNSUCCESSFUL = new SoundToken(Sound.ITEM_BREAK, 1F, 0.5F);
	
	public SoundToken successful = SUCCESSFUL;
	public SoundToken unsuccessful = UNSUCCESSFUL;
	
	/**
	 * @return True if successful, false otherwise
	 */
	public abstract boolean onClick(Player player, Click click);
	
}
