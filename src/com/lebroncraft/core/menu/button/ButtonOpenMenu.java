package com.lebroncraft.core.menu.button;


import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.menu.MenuManager;


public class ButtonOpenMenu
		extends Button {
		
	private MenuManager menuManager;
	private String menuName;
	
	
	public ButtonOpenMenu(MenuManager menuManager, String menuName) {
		this.menuManager = menuManager;
		this.menuName = menuName;
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		menuManager.open(player, menuName);
		return true;
	}
	
}
