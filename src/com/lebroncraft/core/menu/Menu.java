package com.lebroncraft.core.menu;


import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.common.SoundToken;
import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.core.menu.button.Button;
import com.lebroncraft.core.menu.button.ButtonBack;


public abstract class Menu
		implements Listener {
		
	protected SafeMap<Integer, Button> buttons = new SafeMap<>();
	protected SafeMap<Integer, ItemStack> defaultItems = new SafeMap<>();
	
	protected MenuManager menuManager;
	protected Inventory recentInventory;
	protected String title;
	protected String rawName;
	protected int rows;
	protected String prevRawName;
	protected boolean lockItems = true;
	
	public Menu(MenuManager manager, String title, String rawName, int rows, String prevRawName) {
		this.menuManager = manager;
		this.title = title;
		this.rawName = rawName;
		this.rows = rows;
		this.prevRawName = prevRawName;
		
		if (prevRawName != null) {
			addBackButton();
		}
		
		manager.menus.add(this);
		manager.registerEvents(this);
	}
	
	public abstract void addComponents();
	
	public abstract void addItems(Player player, Inventory inventory);
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory clInv = event.getClickedInventory();
		
		if (clInv == null) {
			return;
		}
		
		if (clInv.getTitle()
				.equals(title)) {
			Button clicked = buttons.get(event.getSlot());
			
			if (clicked != null) {
				
				Click click = Click.from(event);
				boolean success = clicked.onClick(player, click);
				
				SoundToken sound = null;
				if (success) {
					sound = clicked.successful;
				} else {
					sound = clicked.unsuccessful;
				}
				
				if (sound != null) {
					UtilSound.playLocal(player, sound);
				}
				
				event.setCancelled(true);
			} else if (lockItems) {
				event.setCancelled(true);
			}
		}
	}
	
	public void addBackButton() {
		ButtonBack button = new ButtonBack(this);
		String display = ( prevRawName.equals("close") ) ? ChatColor.RED + "Close" : ChatColor.BLUE + "Previous page";
		ItemStack item = UtilItem.create(Material.BARRIER, 0, display);
		addButton(0, button, item);
	}
	
	public void open(Player player) {
		recentInventory = Bukkit.createInventory(null, ( rows * 9 ), title);
		for (Entry<Integer, ItemStack> entry : defaultItems.entrySet()) {
			recentInventory.setItem(entry.getKey(), entry.getValue());
		}
		addItems(player, recentInventory);
		player.openInventory(recentInventory);
	}
	
	public void addButton(int slot, Button button) {
		buttons.put(slot, button);
	}
	
	public void addButton(int slot, Button button, ItemStack item) {
		addButton(slot, button);
		addDefaultItem(slot, item);
	}
	
	public void addDefaultItem(int slot, ItemStack item) {
		defaultItems.put(slot, item);
	}
	
	public int getSlot(int column, int row) {
		return ( 9 * row ) + column;
	}
	
	public SafeMap<Integer, Button> getButtons() {
		return buttons;
	}
	
	public SafeMap<Integer, ItemStack> getDefaultItems() {
		return defaultItems;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getRawName() {
		return rawName;
	}
	
	public int getRows() {
		return rows;
	}
	
	public String getPrevRawName() {
		return prevRawName;
	}
	
	public MenuManager getMenuManager() {
		return menuManager;
	}
	
	public void dispose() {
		recentInventory = null;
		menuManager.unregisterEvents(this);
		menuManager.menus.remove(this);
	}
	
}
