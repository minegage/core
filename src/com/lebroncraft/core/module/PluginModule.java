package com.lebroncraft.core.module;


import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Server;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.CommandManager;
import com.lebroncraft.core.common.LazyScheduler;


public abstract class PluginModule
		extends LazyScheduler
		implements Listener {
		
	private boolean enabled = false;
	
	protected String name;
	protected Logger logger;
	
	public PluginModule(String name, JavaPlugin plugin) {
		super(plugin);
		
		this.plugin = plugin;
		this.name = name;
		this.logger = plugin.getLogger();
		
		enable();
	}
	
	public PluginModule(String name, PluginModule module) {
		this(name, module.getPlugin());
	}
	
	public final void enable() {
		logInfo("enabling...");
		onEnable();
		registerEvents(this);
		enabled = true;
	}
	
	public final void disable() {
		logInfo("disabling...");
		onDisable();
		unregisterEvents(this);
		cancelAllTasks();
		enabled = false;
	}
	
	protected void onEnable() {
		// Optional override
	}
	
	protected void onDisable() {
		// Optional override
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void d(Object m) {
		getServer().broadcastMessage(m + "");
	}
	
	public void logInfo(Object info) {
		log(Level.INFO, info);
	}
	
	public void logWarn(Object warning) {
		log(Level.WARNING, warning);
	}
	
	public void logSevere(Object severe) {
		log(Level.SEVERE, severe);
	}
	
	public void log(Level level, Object message) {
		logger.log(level, name + " - " + message);
	}
	
	public Server getServer() {
		return plugin.getServer();
	}
	
	public void registerEvents(Listener listener) {
		getPluginManager().registerEvents(listener, plugin);
	}
	
	public void unregisterEvents(Listener listener) {
		HandlerList.unregisterAll(listener);
	}
	
	public void registerCommand(CommandBase command) {
		CommandManager.instance.addCommand(command);
	}
	
	public PluginManager getPluginManager() {
		return getServer().getPluginManager();
	}
	
	public String getName() {
		return name;
	}
	
	public JavaPlugin getPlugin() {
		return plugin;
	}
	
	public Logger getLogger() {
		return logger;
	}
	
}
