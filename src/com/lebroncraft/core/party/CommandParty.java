package com.lebroncraft.core.party;

import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandModule;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.rank.Rank;

public class CommandParty 
		extends CommandModule<PartyManager> {
	
	public CommandParty(PartyManager partyManager) {
		super(partyManager, Rank.DEVELOPER, "party", "p");
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		C.pRaw(player, "coming soon ;)");
	}

}
