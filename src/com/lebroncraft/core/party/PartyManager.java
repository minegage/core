package com.lebroncraft.core.party;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.java.SafeMap;
import com.lebroncraft.core.module.PluginModule;

public class PartyManager 
		extends PluginModule {
	
	private SafeMap<UUID, List<UUID>> parties = new SafeMap<>();
	
	public PartyManager(JavaPlugin plugin) {
		super("Party Manager", plugin);
		
		//registerCommand(new CommandParty(this));
	}
	
	public void createParty(Player player) {
		parties.put(player.getUniqueId(), new ArrayList<>());
	}
	
	public List<UUID> getPartyMembers(Player owner) {
		return parties.get(owner.getUniqueId());
	}	
	
}
