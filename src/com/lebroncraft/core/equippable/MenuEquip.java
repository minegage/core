package com.lebroncraft.core.equippable;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.equippable.gadget.armour.MenuWardrobe;
import com.lebroncraft.core.equippable.gadget.item.GadgetItem;
import com.lebroncraft.core.equippable.trail.MenuTrail;
import com.lebroncraft.core.menu.Menu;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.menu.button.Button;
import com.lebroncraft.core.menu.button.ButtonCommand;


public class MenuEquip
		extends Menu {
		
	public static final String RAW_NAME = "perk menu";
	private EquippableManager equippableManager;
	
	public MenuEquip(MenuManager manager, EquippableManager equippableManager) {
		super(manager, "Perk Menu", RAW_NAME, 4, "close");
		this.equippableManager = equippableManager;
		addComponents();
	}
	
	@Override
	public void addComponents() {
		List<String> trailLore = new ArrayList<>();
		trailLore.add("");
		trailLore.add(C.cGreen + "Click " + C.cWhite + "to open " + C.cYellow + "trail " + C.cWhite + "menu!");
		
		List<String> petLore = new ArrayList<>();
		petLore.add("");
		petLore.add(C.cGreen + "Click " + C.cWhite + "to open " + C.cYellow + "pet " + C.cWhite + "menu!");
		
		List<String> armourLore = new ArrayList<>();
		armourLore.add("");
		armourLore.add(C.cGreen + "Click " + C.cWhite + "to open " + C.cYellow + "wardrobe" + C.cWhite + "!");
		
		ItemStack trailItem = UtilItem.create(Material.BLAZE_POWDER, C.cBlue + "Trails", trailLore);
		ItemStack petItem = UtilItem.create(Material.BONE, C.cBlue + "Pets", petLore);
		ItemStack armourItem = UtilItem.create(Material.LEATHER, C.cBlue + "Wardrobe", armourLore);
		
		addButton(getSlot(1, 2), new Button() {
			@Override
			public boolean onClick(Player player, Click click) {
				Menu menu = new MenuTrail(menuManager, equippableManager, player);
				menu.open(player);
				return true;
			}
		}, trailItem);
		
		addButton(getSlot(2, 2), new ButtonCommand("pet select"), petItem);
		
		addButton(getSlot(3, 2), new Button() {
			@Override
			public boolean onClick(Player player, Click click) {
				Menu menu = new MenuWardrobe(menuManager, equippableManager, player);
				menu.open(player);
				return true;
			}
		}, armourItem);
		
		for (Equippable gadget : equippableManager.getEquippables()) {
			if (gadget instanceof GadgetItem) {
				addGadget(gadget);
			}
		}
	}
	
	@Override
	public void addItems(Player player, Inventory inventory) {
		// Do nothing
	}
	
	private void addGadget(Equippable gadget) {
		ButtonEquip button = new ButtonEquip(equippableManager, gadget);
		addButton(gadget.getDisplaySlot(), button, gadget.getDisplayItem());
	}
	
}
