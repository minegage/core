package com.lebroncraft.core.equippable.gadget.armour;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilArmour.ArmourSlot;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.equippable.ButtonEquip;
import com.lebroncraft.core.equippable.Equippable;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.equippable.MenuEquip;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.menu.MenuTemp;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;


public class MenuWardrobe
		extends MenuTemp {
		
	private EquippableManager equippableManager;
	
	public MenuWardrobe(MenuManager manager, EquippableManager equippableManager, Player player) {
		super(manager, "Wardrobe", 6, MenuEquip.RAW_NAME, player);
		this.equippableManager = equippableManager;
		addComponents();
	}
	
	@Override
	public void addComponents() {
		for (Equippable gadget : equippableManager.getEquippables()) {
			if (gadget instanceof GadgetArmour) {
				ButtonEquip button = new ButtonEquip(equippableManager, gadget, false);
				addButton(gadget.getDisplaySlot(), button, gadget.getDisplayItem());
			}
		}
		
		int row = 1;
		
		for (ArmourSlot armourSlot : ArmourSlot.values()) {
			List<String> lore = new ArrayList<>();
			lore.add("");
			lore.add(C.iOut + "Click " + C.cReset + "to unequip!");
			ItemStack item = UtilItem.create(Material.REDSTONE_BLOCK, "Armour - " + armourSlot.getName(), lore);
			
			ButtonUnequipArmour button = new ButtonUnequipArmour(equippableManager, armourSlot.getSlot());
			addButton(UtilItem.getSlot(1, row++), button, item);
		}
		
	}
	
	@Override
	public void addItems(Player player, Inventory inventory) {
		// Do nothing
	}
	
	@EventHandler
	public void updateWardrobeInventory(TickEvent event) {
		if (event.isNot(Tick.TICK_1)) {
			return;
		}
		
		for (ItemStack item : player.getOpenInventory()
				.getTopInventory()
				.getContents()) {
				
			if (item == null) {
				continue;
			}
			if (item.getType()
					.equals(Material.AIR)) {
				continue;
			}
			if (!item.hasItemMeta()) {
				continue;
			}
			if (!item.getItemMeta()
					.hasDisplayName()) {
				continue;
			}
			
			if (!item.getItemMeta()
					.getDisplayName()
					.contains("Rainbow")) {
				continue;
			}
			
			equippableManager.getGadgetManager()
					.colourArmour(item, player);
		}
		
		player.updateInventory();
	}
	
}
