package com.lebroncraft.core.equippable.gadget.armour;


import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.equippable.Equippable;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.menu.button.Button;


public class ButtonUnequipArmour
		extends Button {
		
	private EquippableManager equippableManager;
	private int slot;
	
	public ButtonUnequipArmour(EquippableManager equippableManager, int slot) {
		this.equippableManager = equippableManager;
		this.slot = slot;
		
		this.unsuccessful = null;
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		List<Equippable> gadgets = equippableManager.getEquipped(player);
		
		Equippable gadget = null;
		
		for (Equippable g : gadgets) {
			if (g.getEquipSlot() == this.slot) {
				gadget = g;
			}
		}
		
		if (gadget == null) {
			C.pMain(player, "Gadget", "You don't have a gadget equipped in that slot!");
			return false;
		}
		
		equippableManager.unequip(player, gadget, true);
		return true;
	}
	
}
