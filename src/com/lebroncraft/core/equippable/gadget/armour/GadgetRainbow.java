package com.lebroncraft.core.equippable.gadget.armour;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.Color;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.util.UtilArmour.ArmourSlot;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.rank.Rank;


public class GadgetRainbow
		extends GadgetArmour {
		
	public GadgetRainbow(EquippableManager manager, String name, ItemStack item, ArmourSlot slot, int displaySlot, Rank rank) {
		super(manager, name, item, slot, displaySlot, rank);
	}
	
	public static List<Color> colourSequence = new ArrayList<>();
	
	static {
		double shades = 50D;
		double step = 1.0 / shades;
		
		for (double hue = 0.0; hue <= 1.0; hue += step) {
			java.awt.Color color = java.awt.Color.getHSBColor((float) hue, 1F, 1F);
			Color bukkitColor = Color.fromRGB(color.getRed(), color.getGreen(), color.getBlue());
			colourSequence.add(bukkitColor);
		}
	}
	
}
