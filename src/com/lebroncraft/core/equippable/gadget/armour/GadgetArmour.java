package com.lebroncraft.core.equippable.gadget.armour;


import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.util.UtilArmour.ArmourSlot;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.equippable.gadget.Gadget;
import com.lebroncraft.core.rank.Rank;


public class GadgetArmour
		extends Gadget {
		
	public GadgetArmour(EquippableManager manager, String name, ItemStack item, ArmourSlot slot, int displaySlot, Rank rank) {
		super(manager, name, item, displaySlot, rank, slot.getSlot());
	}
	
}
