package com.lebroncraft.core.equippable.gadget.item;


import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.equippable.gadget.Gadget;
import com.lebroncraft.core.rank.Rank;


public abstract class GadgetItem
		extends Gadget {
		
	public static final int EQUIP_SLOT = 2;
	
	public GadgetItem(EquippableManager manager, String name, ItemStack item, int displaySlot, Rank rank) {
		super(manager, name, item, displaySlot, rank, EQUIP_SLOT);
	}
	
	public abstract void use(Player player);
	
}
