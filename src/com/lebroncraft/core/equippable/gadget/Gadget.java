package com.lebroncraft.core.equippable.gadget;


import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.equippable.Equippable;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.rank.Rank;


public abstract class Gadget
		extends Equippable {
		
	public static final String GADGET_METADATA_KEY = "fromgadget";
	
	public Gadget(EquippableManager manager, String name, ItemStack item, int displaySlot, Rank rank, int equipSlot) {
		super(manager, "Gadget", name, item, displaySlot, rank, equipSlot);
	}
	
}
