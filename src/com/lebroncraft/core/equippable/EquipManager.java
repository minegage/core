package com.lebroncraft.core.equippable;


import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.move.MoveManager;


public class EquipManager
		extends PluginModule {
		
	private EquippableManager equippableManager;
	private MenuEquip menuEquip;
	
	public EquipManager(JavaPlugin plugin, MenuManager menuManager, MoveManager moveManager) {
		super("Equip Manager", plugin);
		
		this.equippableManager = new EquippableManager(plugin, moveManager);
		this.menuEquip = new MenuEquip(menuManager, equippableManager);
	}
	
	public EquippableManager getEquippableManager() {
		return equippableManager;
	}
	
	public MenuEquip getMenuEquip() {
		return menuEquip;
	}
	
}
