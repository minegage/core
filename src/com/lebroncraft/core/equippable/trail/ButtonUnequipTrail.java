package com.lebroncraft.core.equippable.trail;


import org.bukkit.entity.Player;

import com.lebroncraft.core.common.Click;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.menu.button.Button;


public class ButtonUnequipTrail
		extends Button {
		
	private EquippableManager equippableManager;
	private MenuManager menuManager;
	private Trail trail;
	
	public ButtonUnequipTrail(EquippableManager equippableManager, MenuManager menuManager, Trail trail) {
		this.equippableManager = equippableManager;
		this.menuManager = menuManager;
		this.trail = trail;
	}
	
	@Override
	public boolean onClick(Player player, Click click) {
		equippableManager.unequip(player, trail, true);
		MenuTrail menu = new MenuTrail(menuManager, equippableManager, player);
		menu.open(player);
		return true;
	}
}
