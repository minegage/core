package com.lebroncraft.core.equippable.trail;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.equippable.Equippable;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.rank.Rank;

public abstract class Trail 
		extends Equippable {
	
	public Trail(EquippableManager manager, String name, ItemStack displayItem, int displaySlot, Rank rank) {
		super(manager, "Trail", name, displayItem, displaySlot, rank);
	}
	
	public abstract void play(Player player, Location location);
	
	public abstract Location getLocation(Player player);
	
}
