package com.lebroncraft.core.equippable.trail;


import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.equippable.Equippable;
import com.lebroncraft.core.equippable.EquippableManager;
import com.lebroncraft.core.equippable.MenuEquip;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.menu.MenuTemp;


public class MenuTrail
		extends MenuTemp {

	public static final int[] DISPLAY_SLOTS = {4, 3, 5, 2, 6, 1, 7};
	
	private EquippableManager equippableManager;
	
	public MenuTrail(MenuManager menuManager, EquippableManager equippableManager, Player player) {
		super(menuManager, "Trails", 6, MenuEquip.RAW_NAME, player);
		this.equippableManager = equippableManager;
		addComponents();
	}
	
	@Override
	public void addComponents() {
		// Do nothing
	}
	
	@Override
	public void addItems(Player player, Inventory inventory) {
		int displayIndex = 0;
		
		for (Equippable equippable : equippableManager.getEquippables()) {
			if (!( equippable instanceof Trail )) {
				continue;
			}
			
			Trail trail = (Trail) equippable;
			
			ItemStack item = trail.getDisplayItem();
			ButtonEquipTrail button = new ButtonEquipTrail(equippableManager, menuManager, trail);
			addButton(trail.getDisplaySlot(), button);
			inventory.setItem(trail.getDisplaySlot(), trail.getDisplayItem());
			
			if (trail.getEquipped().contains(player.getUniqueId())) {
				ItemStack unequipItem = item.clone();
				UtilItem.stripLore(unequipItem);
				UtilItem.addLore(unequipItem, new ArrayList<>(Arrays.asList("", C.iOut + "Click " + C.iMain + "to unequip!")));
				
				int displaySlot = DISPLAY_SLOTS[displayIndex++];
				ButtonUnequipTrail unequipButton = new ButtonUnequipTrail(equippableManager, menuManager, trail);
				addButton(displaySlot, unequipButton);
				inventory.setItem(displaySlot, unequipItem);
			}
		}
	}
	
}
